package bitutil_test

import (
	"testing"

	"codeberg.org/gruf/go-bitutil"
)

func TestFlags8Get(t *testing.T) {
	var mask, flags bitutil.Flags8

	mask = bitutil.Flags8(1) << 0

	flags = 0

	flags |= mask
	if !flags.Get(0) {
		t.Error("failed .Get() set Flags8 bit at index 0")
	}

	flags = ^bitutil.Flags8(0)

	flags &= ^mask
	if flags.Get(0) {
		t.Error("failed .Get() unset Flags8 bit at index 0")
	}

	flags = 0

	flags |= mask
	if !flags.Get0() {
		t.Error("failed .Get0() set Flags8 bit at index 0")
	}

	flags = ^bitutil.Flags8(0)

	flags &= ^mask
	if flags.Get0() {
		t.Error("failed .Get0() unset Flags8 bit at index 0")
	}

	mask = bitutil.Flags8(1) << 1

	flags = 0

	flags |= mask
	if !flags.Get(1) {
		t.Error("failed .Get() set Flags8 bit at index 1")
	}

	flags = ^bitutil.Flags8(0)

	flags &= ^mask
	if flags.Get(1) {
		t.Error("failed .Get() unset Flags8 bit at index 1")
	}

	flags = 0

	flags |= mask
	if !flags.Get1() {
		t.Error("failed .Get1() set Flags8 bit at index 1")
	}

	flags = ^bitutil.Flags8(0)

	flags &= ^mask
	if flags.Get1() {
		t.Error("failed .Get1() unset Flags8 bit at index 1")
	}

	mask = bitutil.Flags8(1) << 2

	flags = 0

	flags |= mask
	if !flags.Get(2) {
		t.Error("failed .Get() set Flags8 bit at index 2")
	}

	flags = ^bitutil.Flags8(0)

	flags &= ^mask
	if flags.Get(2) {
		t.Error("failed .Get() unset Flags8 bit at index 2")
	}

	flags = 0

	flags |= mask
	if !flags.Get2() {
		t.Error("failed .Get2() set Flags8 bit at index 2")
	}

	flags = ^bitutil.Flags8(0)

	flags &= ^mask
	if flags.Get2() {
		t.Error("failed .Get2() unset Flags8 bit at index 2")
	}

	mask = bitutil.Flags8(1) << 3

	flags = 0

	flags |= mask
	if !flags.Get(3) {
		t.Error("failed .Get() set Flags8 bit at index 3")
	}

	flags = ^bitutil.Flags8(0)

	flags &= ^mask
	if flags.Get(3) {
		t.Error("failed .Get() unset Flags8 bit at index 3")
	}

	flags = 0

	flags |= mask
	if !flags.Get3() {
		t.Error("failed .Get3() set Flags8 bit at index 3")
	}

	flags = ^bitutil.Flags8(0)

	flags &= ^mask
	if flags.Get3() {
		t.Error("failed .Get3() unset Flags8 bit at index 3")
	}

	mask = bitutil.Flags8(1) << 4

	flags = 0

	flags |= mask
	if !flags.Get(4) {
		t.Error("failed .Get() set Flags8 bit at index 4")
	}

	flags = ^bitutil.Flags8(0)

	flags &= ^mask
	if flags.Get(4) {
		t.Error("failed .Get() unset Flags8 bit at index 4")
	}

	flags = 0

	flags |= mask
	if !flags.Get4() {
		t.Error("failed .Get4() set Flags8 bit at index 4")
	}

	flags = ^bitutil.Flags8(0)

	flags &= ^mask
	if flags.Get4() {
		t.Error("failed .Get4() unset Flags8 bit at index 4")
	}

	mask = bitutil.Flags8(1) << 5

	flags = 0

	flags |= mask
	if !flags.Get(5) {
		t.Error("failed .Get() set Flags8 bit at index 5")
	}

	flags = ^bitutil.Flags8(0)

	flags &= ^mask
	if flags.Get(5) {
		t.Error("failed .Get() unset Flags8 bit at index 5")
	}

	flags = 0

	flags |= mask
	if !flags.Get5() {
		t.Error("failed .Get5() set Flags8 bit at index 5")
	}

	flags = ^bitutil.Flags8(0)

	flags &= ^mask
	if flags.Get5() {
		t.Error("failed .Get5() unset Flags8 bit at index 5")
	}

	mask = bitutil.Flags8(1) << 6

	flags = 0

	flags |= mask
	if !flags.Get(6) {
		t.Error("failed .Get() set Flags8 bit at index 6")
	}

	flags = ^bitutil.Flags8(0)

	flags &= ^mask
	if flags.Get(6) {
		t.Error("failed .Get() unset Flags8 bit at index 6")
	}

	flags = 0

	flags |= mask
	if !flags.Get6() {
		t.Error("failed .Get6() set Flags8 bit at index 6")
	}

	flags = ^bitutil.Flags8(0)

	flags &= ^mask
	if flags.Get6() {
		t.Error("failed .Get6() unset Flags8 bit at index 6")
	}

	mask = bitutil.Flags8(1) << 7

	flags = 0

	flags |= mask
	if !flags.Get(7) {
		t.Error("failed .Get() set Flags8 bit at index 7")
	}

	flags = ^bitutil.Flags8(0)

	flags &= ^mask
	if flags.Get(7) {
		t.Error("failed .Get() unset Flags8 bit at index 7")
	}

	flags = 0

	flags |= mask
	if !flags.Get7() {
		t.Error("failed .Get7() set Flags8 bit at index 7")
	}

	flags = ^bitutil.Flags8(0)

	flags &= ^mask
	if flags.Get7() {
		t.Error("failed .Get7() unset Flags8 bit at index 7")
	}
}

func TestFlags8Set(t *testing.T) {
	var mask, flags bitutil.Flags8

	mask = bitutil.Flags8(1) << 0

	flags = 0

	flags = flags.Set(0)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags8 bit at index 0")
	}

	flags = 0

	flags = flags.Set0()
	if flags&mask == 0 {
		t.Error("failed .Set0() Flags8 bit at index 0")
	}

	mask = bitutil.Flags8(1) << 1

	flags = 0

	flags = flags.Set(1)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags8 bit at index 1")
	}

	flags = 0

	flags = flags.Set1()
	if flags&mask == 0 {
		t.Error("failed .Set1() Flags8 bit at index 1")
	}

	mask = bitutil.Flags8(1) << 2

	flags = 0

	flags = flags.Set(2)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags8 bit at index 2")
	}

	flags = 0

	flags = flags.Set2()
	if flags&mask == 0 {
		t.Error("failed .Set2() Flags8 bit at index 2")
	}

	mask = bitutil.Flags8(1) << 3

	flags = 0

	flags = flags.Set(3)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags8 bit at index 3")
	}

	flags = 0

	flags = flags.Set3()
	if flags&mask == 0 {
		t.Error("failed .Set3() Flags8 bit at index 3")
	}

	mask = bitutil.Flags8(1) << 4

	flags = 0

	flags = flags.Set(4)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags8 bit at index 4")
	}

	flags = 0

	flags = flags.Set4()
	if flags&mask == 0 {
		t.Error("failed .Set4() Flags8 bit at index 4")
	}

	mask = bitutil.Flags8(1) << 5

	flags = 0

	flags = flags.Set(5)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags8 bit at index 5")
	}

	flags = 0

	flags = flags.Set5()
	if flags&mask == 0 {
		t.Error("failed .Set5() Flags8 bit at index 5")
	}

	mask = bitutil.Flags8(1) << 6

	flags = 0

	flags = flags.Set(6)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags8 bit at index 6")
	}

	flags = 0

	flags = flags.Set6()
	if flags&mask == 0 {
		t.Error("failed .Set6() Flags8 bit at index 6")
	}

	mask = bitutil.Flags8(1) << 7

	flags = 0

	flags = flags.Set(7)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags8 bit at index 7")
	}

	flags = 0

	flags = flags.Set7()
	if flags&mask == 0 {
		t.Error("failed .Set7() Flags8 bit at index 7")
	}
}

func TestFlags8Unset(t *testing.T) {
	var mask, flags bitutil.Flags8

	mask = bitutil.Flags8(1) << 0

	flags = ^bitutil.Flags8(0)

	flags = flags.Unset(0)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags8 bit at index 0")
	}

	flags = ^bitutil.Flags8(0)

	flags = flags.Unset0()
	if flags&mask != 0 {
		t.Error("failed .Unset0() Flags8 bit at index 0")
	}

	mask = bitutil.Flags8(1) << 1

	flags = ^bitutil.Flags8(0)

	flags = flags.Unset(1)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags8 bit at index 1")
	}

	flags = ^bitutil.Flags8(0)

	flags = flags.Unset1()
	if flags&mask != 0 {
		t.Error("failed .Unset1() Flags8 bit at index 1")
	}

	mask = bitutil.Flags8(1) << 2

	flags = ^bitutil.Flags8(0)

	flags = flags.Unset(2)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags8 bit at index 2")
	}

	flags = ^bitutil.Flags8(0)

	flags = flags.Unset2()
	if flags&mask != 0 {
		t.Error("failed .Unset2() Flags8 bit at index 2")
	}

	mask = bitutil.Flags8(1) << 3

	flags = ^bitutil.Flags8(0)

	flags = flags.Unset(3)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags8 bit at index 3")
	}

	flags = ^bitutil.Flags8(0)

	flags = flags.Unset3()
	if flags&mask != 0 {
		t.Error("failed .Unset3() Flags8 bit at index 3")
	}

	mask = bitutil.Flags8(1) << 4

	flags = ^bitutil.Flags8(0)

	flags = flags.Unset(4)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags8 bit at index 4")
	}

	flags = ^bitutil.Flags8(0)

	flags = flags.Unset4()
	if flags&mask != 0 {
		t.Error("failed .Unset4() Flags8 bit at index 4")
	}

	mask = bitutil.Flags8(1) << 5

	flags = ^bitutil.Flags8(0)

	flags = flags.Unset(5)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags8 bit at index 5")
	}

	flags = ^bitutil.Flags8(0)

	flags = flags.Unset5()
	if flags&mask != 0 {
		t.Error("failed .Unset5() Flags8 bit at index 5")
	}

	mask = bitutil.Flags8(1) << 6

	flags = ^bitutil.Flags8(0)

	flags = flags.Unset(6)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags8 bit at index 6")
	}

	flags = ^bitutil.Flags8(0)

	flags = flags.Unset6()
	if flags&mask != 0 {
		t.Error("failed .Unset6() Flags8 bit at index 6")
	}

	mask = bitutil.Flags8(1) << 7

	flags = ^bitutil.Flags8(0)

	flags = flags.Unset(7)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags8 bit at index 7")
	}

	flags = ^bitutil.Flags8(0)

	flags = flags.Unset7()
	if flags&mask != 0 {
		t.Error("failed .Unset7() Flags8 bit at index 7")
	}
}

func TestFlags16Get(t *testing.T) {
	var mask, flags bitutil.Flags16

	mask = bitutil.Flags16(1) << 0

	flags = 0

	flags |= mask
	if !flags.Get(0) {
		t.Error("failed .Get() set Flags16 bit at index 0")
	}

	flags = ^bitutil.Flags16(0)

	flags &= ^mask
	if flags.Get(0) {
		t.Error("failed .Get() unset Flags16 bit at index 0")
	}

	flags = 0

	flags |= mask
	if !flags.Get0() {
		t.Error("failed .Get0() set Flags16 bit at index 0")
	}

	flags = ^bitutil.Flags16(0)

	flags &= ^mask
	if flags.Get0() {
		t.Error("failed .Get0() unset Flags16 bit at index 0")
	}

	mask = bitutil.Flags16(1) << 1

	flags = 0

	flags |= mask
	if !flags.Get(1) {
		t.Error("failed .Get() set Flags16 bit at index 1")
	}

	flags = ^bitutil.Flags16(0)

	flags &= ^mask
	if flags.Get(1) {
		t.Error("failed .Get() unset Flags16 bit at index 1")
	}

	flags = 0

	flags |= mask
	if !flags.Get1() {
		t.Error("failed .Get1() set Flags16 bit at index 1")
	}

	flags = ^bitutil.Flags16(0)

	flags &= ^mask
	if flags.Get1() {
		t.Error("failed .Get1() unset Flags16 bit at index 1")
	}

	mask = bitutil.Flags16(1) << 2

	flags = 0

	flags |= mask
	if !flags.Get(2) {
		t.Error("failed .Get() set Flags16 bit at index 2")
	}

	flags = ^bitutil.Flags16(0)

	flags &= ^mask
	if flags.Get(2) {
		t.Error("failed .Get() unset Flags16 bit at index 2")
	}

	flags = 0

	flags |= mask
	if !flags.Get2() {
		t.Error("failed .Get2() set Flags16 bit at index 2")
	}

	flags = ^bitutil.Flags16(0)

	flags &= ^mask
	if flags.Get2() {
		t.Error("failed .Get2() unset Flags16 bit at index 2")
	}

	mask = bitutil.Flags16(1) << 3

	flags = 0

	flags |= mask
	if !flags.Get(3) {
		t.Error("failed .Get() set Flags16 bit at index 3")
	}

	flags = ^bitutil.Flags16(0)

	flags &= ^mask
	if flags.Get(3) {
		t.Error("failed .Get() unset Flags16 bit at index 3")
	}

	flags = 0

	flags |= mask
	if !flags.Get3() {
		t.Error("failed .Get3() set Flags16 bit at index 3")
	}

	flags = ^bitutil.Flags16(0)

	flags &= ^mask
	if flags.Get3() {
		t.Error("failed .Get3() unset Flags16 bit at index 3")
	}

	mask = bitutil.Flags16(1) << 4

	flags = 0

	flags |= mask
	if !flags.Get(4) {
		t.Error("failed .Get() set Flags16 bit at index 4")
	}

	flags = ^bitutil.Flags16(0)

	flags &= ^mask
	if flags.Get(4) {
		t.Error("failed .Get() unset Flags16 bit at index 4")
	}

	flags = 0

	flags |= mask
	if !flags.Get4() {
		t.Error("failed .Get4() set Flags16 bit at index 4")
	}

	flags = ^bitutil.Flags16(0)

	flags &= ^mask
	if flags.Get4() {
		t.Error("failed .Get4() unset Flags16 bit at index 4")
	}

	mask = bitutil.Flags16(1) << 5

	flags = 0

	flags |= mask
	if !flags.Get(5) {
		t.Error("failed .Get() set Flags16 bit at index 5")
	}

	flags = ^bitutil.Flags16(0)

	flags &= ^mask
	if flags.Get(5) {
		t.Error("failed .Get() unset Flags16 bit at index 5")
	}

	flags = 0

	flags |= mask
	if !flags.Get5() {
		t.Error("failed .Get5() set Flags16 bit at index 5")
	}

	flags = ^bitutil.Flags16(0)

	flags &= ^mask
	if flags.Get5() {
		t.Error("failed .Get5() unset Flags16 bit at index 5")
	}

	mask = bitutil.Flags16(1) << 6

	flags = 0

	flags |= mask
	if !flags.Get(6) {
		t.Error("failed .Get() set Flags16 bit at index 6")
	}

	flags = ^bitutil.Flags16(0)

	flags &= ^mask
	if flags.Get(6) {
		t.Error("failed .Get() unset Flags16 bit at index 6")
	}

	flags = 0

	flags |= mask
	if !flags.Get6() {
		t.Error("failed .Get6() set Flags16 bit at index 6")
	}

	flags = ^bitutil.Flags16(0)

	flags &= ^mask
	if flags.Get6() {
		t.Error("failed .Get6() unset Flags16 bit at index 6")
	}

	mask = bitutil.Flags16(1) << 7

	flags = 0

	flags |= mask
	if !flags.Get(7) {
		t.Error("failed .Get() set Flags16 bit at index 7")
	}

	flags = ^bitutil.Flags16(0)

	flags &= ^mask
	if flags.Get(7) {
		t.Error("failed .Get() unset Flags16 bit at index 7")
	}

	flags = 0

	flags |= mask
	if !flags.Get7() {
		t.Error("failed .Get7() set Flags16 bit at index 7")
	}

	flags = ^bitutil.Flags16(0)

	flags &= ^mask
	if flags.Get7() {
		t.Error("failed .Get7() unset Flags16 bit at index 7")
	}

	mask = bitutil.Flags16(1) << 8

	flags = 0

	flags |= mask
	if !flags.Get(8) {
		t.Error("failed .Get() set Flags16 bit at index 8")
	}

	flags = ^bitutil.Flags16(0)

	flags &= ^mask
	if flags.Get(8) {
		t.Error("failed .Get() unset Flags16 bit at index 8")
	}

	flags = 0

	flags |= mask
	if !flags.Get8() {
		t.Error("failed .Get8() set Flags16 bit at index 8")
	}

	flags = ^bitutil.Flags16(0)

	flags &= ^mask
	if flags.Get8() {
		t.Error("failed .Get8() unset Flags16 bit at index 8")
	}

	mask = bitutil.Flags16(1) << 9

	flags = 0

	flags |= mask
	if !flags.Get(9) {
		t.Error("failed .Get() set Flags16 bit at index 9")
	}

	flags = ^bitutil.Flags16(0)

	flags &= ^mask
	if flags.Get(9) {
		t.Error("failed .Get() unset Flags16 bit at index 9")
	}

	flags = 0

	flags |= mask
	if !flags.Get9() {
		t.Error("failed .Get9() set Flags16 bit at index 9")
	}

	flags = ^bitutil.Flags16(0)

	flags &= ^mask
	if flags.Get9() {
		t.Error("failed .Get9() unset Flags16 bit at index 9")
	}

	mask = bitutil.Flags16(1) << 10

	flags = 0

	flags |= mask
	if !flags.Get(10) {
		t.Error("failed .Get() set Flags16 bit at index 10")
	}

	flags = ^bitutil.Flags16(0)

	flags &= ^mask
	if flags.Get(10) {
		t.Error("failed .Get() unset Flags16 bit at index 10")
	}

	flags = 0

	flags |= mask
	if !flags.Get10() {
		t.Error("failed .Get10() set Flags16 bit at index 10")
	}

	flags = ^bitutil.Flags16(0)

	flags &= ^mask
	if flags.Get10() {
		t.Error("failed .Get10() unset Flags16 bit at index 10")
	}

	mask = bitutil.Flags16(1) << 11

	flags = 0

	flags |= mask
	if !flags.Get(11) {
		t.Error("failed .Get() set Flags16 bit at index 11")
	}

	flags = ^bitutil.Flags16(0)

	flags &= ^mask
	if flags.Get(11) {
		t.Error("failed .Get() unset Flags16 bit at index 11")
	}

	flags = 0

	flags |= mask
	if !flags.Get11() {
		t.Error("failed .Get11() set Flags16 bit at index 11")
	}

	flags = ^bitutil.Flags16(0)

	flags &= ^mask
	if flags.Get11() {
		t.Error("failed .Get11() unset Flags16 bit at index 11")
	}

	mask = bitutil.Flags16(1) << 12

	flags = 0

	flags |= mask
	if !flags.Get(12) {
		t.Error("failed .Get() set Flags16 bit at index 12")
	}

	flags = ^bitutil.Flags16(0)

	flags &= ^mask
	if flags.Get(12) {
		t.Error("failed .Get() unset Flags16 bit at index 12")
	}

	flags = 0

	flags |= mask
	if !flags.Get12() {
		t.Error("failed .Get12() set Flags16 bit at index 12")
	}

	flags = ^bitutil.Flags16(0)

	flags &= ^mask
	if flags.Get12() {
		t.Error("failed .Get12() unset Flags16 bit at index 12")
	}

	mask = bitutil.Flags16(1) << 13

	flags = 0

	flags |= mask
	if !flags.Get(13) {
		t.Error("failed .Get() set Flags16 bit at index 13")
	}

	flags = ^bitutil.Flags16(0)

	flags &= ^mask
	if flags.Get(13) {
		t.Error("failed .Get() unset Flags16 bit at index 13")
	}

	flags = 0

	flags |= mask
	if !flags.Get13() {
		t.Error("failed .Get13() set Flags16 bit at index 13")
	}

	flags = ^bitutil.Flags16(0)

	flags &= ^mask
	if flags.Get13() {
		t.Error("failed .Get13() unset Flags16 bit at index 13")
	}

	mask = bitutil.Flags16(1) << 14

	flags = 0

	flags |= mask
	if !flags.Get(14) {
		t.Error("failed .Get() set Flags16 bit at index 14")
	}

	flags = ^bitutil.Flags16(0)

	flags &= ^mask
	if flags.Get(14) {
		t.Error("failed .Get() unset Flags16 bit at index 14")
	}

	flags = 0

	flags |= mask
	if !flags.Get14() {
		t.Error("failed .Get14() set Flags16 bit at index 14")
	}

	flags = ^bitutil.Flags16(0)

	flags &= ^mask
	if flags.Get14() {
		t.Error("failed .Get14() unset Flags16 bit at index 14")
	}

	mask = bitutil.Flags16(1) << 15

	flags = 0

	flags |= mask
	if !flags.Get(15) {
		t.Error("failed .Get() set Flags16 bit at index 15")
	}

	flags = ^bitutil.Flags16(0)

	flags &= ^mask
	if flags.Get(15) {
		t.Error("failed .Get() unset Flags16 bit at index 15")
	}

	flags = 0

	flags |= mask
	if !flags.Get15() {
		t.Error("failed .Get15() set Flags16 bit at index 15")
	}

	flags = ^bitutil.Flags16(0)

	flags &= ^mask
	if flags.Get15() {
		t.Error("failed .Get15() unset Flags16 bit at index 15")
	}
}

func TestFlags16Set(t *testing.T) {
	var mask, flags bitutil.Flags16

	mask = bitutil.Flags16(1) << 0

	flags = 0

	flags = flags.Set(0)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags16 bit at index 0")
	}

	flags = 0

	flags = flags.Set0()
	if flags&mask == 0 {
		t.Error("failed .Set0() Flags16 bit at index 0")
	}

	mask = bitutil.Flags16(1) << 1

	flags = 0

	flags = flags.Set(1)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags16 bit at index 1")
	}

	flags = 0

	flags = flags.Set1()
	if flags&mask == 0 {
		t.Error("failed .Set1() Flags16 bit at index 1")
	}

	mask = bitutil.Flags16(1) << 2

	flags = 0

	flags = flags.Set(2)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags16 bit at index 2")
	}

	flags = 0

	flags = flags.Set2()
	if flags&mask == 0 {
		t.Error("failed .Set2() Flags16 bit at index 2")
	}

	mask = bitutil.Flags16(1) << 3

	flags = 0

	flags = flags.Set(3)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags16 bit at index 3")
	}

	flags = 0

	flags = flags.Set3()
	if flags&mask == 0 {
		t.Error("failed .Set3() Flags16 bit at index 3")
	}

	mask = bitutil.Flags16(1) << 4

	flags = 0

	flags = flags.Set(4)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags16 bit at index 4")
	}

	flags = 0

	flags = flags.Set4()
	if flags&mask == 0 {
		t.Error("failed .Set4() Flags16 bit at index 4")
	}

	mask = bitutil.Flags16(1) << 5

	flags = 0

	flags = flags.Set(5)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags16 bit at index 5")
	}

	flags = 0

	flags = flags.Set5()
	if flags&mask == 0 {
		t.Error("failed .Set5() Flags16 bit at index 5")
	}

	mask = bitutil.Flags16(1) << 6

	flags = 0

	flags = flags.Set(6)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags16 bit at index 6")
	}

	flags = 0

	flags = flags.Set6()
	if flags&mask == 0 {
		t.Error("failed .Set6() Flags16 bit at index 6")
	}

	mask = bitutil.Flags16(1) << 7

	flags = 0

	flags = flags.Set(7)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags16 bit at index 7")
	}

	flags = 0

	flags = flags.Set7()
	if flags&mask == 0 {
		t.Error("failed .Set7() Flags16 bit at index 7")
	}

	mask = bitutil.Flags16(1) << 8

	flags = 0

	flags = flags.Set(8)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags16 bit at index 8")
	}

	flags = 0

	flags = flags.Set8()
	if flags&mask == 0 {
		t.Error("failed .Set8() Flags16 bit at index 8")
	}

	mask = bitutil.Flags16(1) << 9

	flags = 0

	flags = flags.Set(9)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags16 bit at index 9")
	}

	flags = 0

	flags = flags.Set9()
	if flags&mask == 0 {
		t.Error("failed .Set9() Flags16 bit at index 9")
	}

	mask = bitutil.Flags16(1) << 10

	flags = 0

	flags = flags.Set(10)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags16 bit at index 10")
	}

	flags = 0

	flags = flags.Set10()
	if flags&mask == 0 {
		t.Error("failed .Set10() Flags16 bit at index 10")
	}

	mask = bitutil.Flags16(1) << 11

	flags = 0

	flags = flags.Set(11)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags16 bit at index 11")
	}

	flags = 0

	flags = flags.Set11()
	if flags&mask == 0 {
		t.Error("failed .Set11() Flags16 bit at index 11")
	}

	mask = bitutil.Flags16(1) << 12

	flags = 0

	flags = flags.Set(12)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags16 bit at index 12")
	}

	flags = 0

	flags = flags.Set12()
	if flags&mask == 0 {
		t.Error("failed .Set12() Flags16 bit at index 12")
	}

	mask = bitutil.Flags16(1) << 13

	flags = 0

	flags = flags.Set(13)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags16 bit at index 13")
	}

	flags = 0

	flags = flags.Set13()
	if flags&mask == 0 {
		t.Error("failed .Set13() Flags16 bit at index 13")
	}

	mask = bitutil.Flags16(1) << 14

	flags = 0

	flags = flags.Set(14)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags16 bit at index 14")
	}

	flags = 0

	flags = flags.Set14()
	if flags&mask == 0 {
		t.Error("failed .Set14() Flags16 bit at index 14")
	}

	mask = bitutil.Flags16(1) << 15

	flags = 0

	flags = flags.Set(15)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags16 bit at index 15")
	}

	flags = 0

	flags = flags.Set15()
	if flags&mask == 0 {
		t.Error("failed .Set15() Flags16 bit at index 15")
	}
}

func TestFlags16Unset(t *testing.T) {
	var mask, flags bitutil.Flags16

	mask = bitutil.Flags16(1) << 0

	flags = ^bitutil.Flags16(0)

	flags = flags.Unset(0)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags16 bit at index 0")
	}

	flags = ^bitutil.Flags16(0)

	flags = flags.Unset0()
	if flags&mask != 0 {
		t.Error("failed .Unset0() Flags16 bit at index 0")
	}

	mask = bitutil.Flags16(1) << 1

	flags = ^bitutil.Flags16(0)

	flags = flags.Unset(1)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags16 bit at index 1")
	}

	flags = ^bitutil.Flags16(0)

	flags = flags.Unset1()
	if flags&mask != 0 {
		t.Error("failed .Unset1() Flags16 bit at index 1")
	}

	mask = bitutil.Flags16(1) << 2

	flags = ^bitutil.Flags16(0)

	flags = flags.Unset(2)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags16 bit at index 2")
	}

	flags = ^bitutil.Flags16(0)

	flags = flags.Unset2()
	if flags&mask != 0 {
		t.Error("failed .Unset2() Flags16 bit at index 2")
	}

	mask = bitutil.Flags16(1) << 3

	flags = ^bitutil.Flags16(0)

	flags = flags.Unset(3)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags16 bit at index 3")
	}

	flags = ^bitutil.Flags16(0)

	flags = flags.Unset3()
	if flags&mask != 0 {
		t.Error("failed .Unset3() Flags16 bit at index 3")
	}

	mask = bitutil.Flags16(1) << 4

	flags = ^bitutil.Flags16(0)

	flags = flags.Unset(4)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags16 bit at index 4")
	}

	flags = ^bitutil.Flags16(0)

	flags = flags.Unset4()
	if flags&mask != 0 {
		t.Error("failed .Unset4() Flags16 bit at index 4")
	}

	mask = bitutil.Flags16(1) << 5

	flags = ^bitutil.Flags16(0)

	flags = flags.Unset(5)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags16 bit at index 5")
	}

	flags = ^bitutil.Flags16(0)

	flags = flags.Unset5()
	if flags&mask != 0 {
		t.Error("failed .Unset5() Flags16 bit at index 5")
	}

	mask = bitutil.Flags16(1) << 6

	flags = ^bitutil.Flags16(0)

	flags = flags.Unset(6)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags16 bit at index 6")
	}

	flags = ^bitutil.Flags16(0)

	flags = flags.Unset6()
	if flags&mask != 0 {
		t.Error("failed .Unset6() Flags16 bit at index 6")
	}

	mask = bitutil.Flags16(1) << 7

	flags = ^bitutil.Flags16(0)

	flags = flags.Unset(7)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags16 bit at index 7")
	}

	flags = ^bitutil.Flags16(0)

	flags = flags.Unset7()
	if flags&mask != 0 {
		t.Error("failed .Unset7() Flags16 bit at index 7")
	}

	mask = bitutil.Flags16(1) << 8

	flags = ^bitutil.Flags16(0)

	flags = flags.Unset(8)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags16 bit at index 8")
	}

	flags = ^bitutil.Flags16(0)

	flags = flags.Unset8()
	if flags&mask != 0 {
		t.Error("failed .Unset8() Flags16 bit at index 8")
	}

	mask = bitutil.Flags16(1) << 9

	flags = ^bitutil.Flags16(0)

	flags = flags.Unset(9)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags16 bit at index 9")
	}

	flags = ^bitutil.Flags16(0)

	flags = flags.Unset9()
	if flags&mask != 0 {
		t.Error("failed .Unset9() Flags16 bit at index 9")
	}

	mask = bitutil.Flags16(1) << 10

	flags = ^bitutil.Flags16(0)

	flags = flags.Unset(10)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags16 bit at index 10")
	}

	flags = ^bitutil.Flags16(0)

	flags = flags.Unset10()
	if flags&mask != 0 {
		t.Error("failed .Unset10() Flags16 bit at index 10")
	}

	mask = bitutil.Flags16(1) << 11

	flags = ^bitutil.Flags16(0)

	flags = flags.Unset(11)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags16 bit at index 11")
	}

	flags = ^bitutil.Flags16(0)

	flags = flags.Unset11()
	if flags&mask != 0 {
		t.Error("failed .Unset11() Flags16 bit at index 11")
	}

	mask = bitutil.Flags16(1) << 12

	flags = ^bitutil.Flags16(0)

	flags = flags.Unset(12)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags16 bit at index 12")
	}

	flags = ^bitutil.Flags16(0)

	flags = flags.Unset12()
	if flags&mask != 0 {
		t.Error("failed .Unset12() Flags16 bit at index 12")
	}

	mask = bitutil.Flags16(1) << 13

	flags = ^bitutil.Flags16(0)

	flags = flags.Unset(13)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags16 bit at index 13")
	}

	flags = ^bitutil.Flags16(0)

	flags = flags.Unset13()
	if flags&mask != 0 {
		t.Error("failed .Unset13() Flags16 bit at index 13")
	}

	mask = bitutil.Flags16(1) << 14

	flags = ^bitutil.Flags16(0)

	flags = flags.Unset(14)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags16 bit at index 14")
	}

	flags = ^bitutil.Flags16(0)

	flags = flags.Unset14()
	if flags&mask != 0 {
		t.Error("failed .Unset14() Flags16 bit at index 14")
	}

	mask = bitutil.Flags16(1) << 15

	flags = ^bitutil.Flags16(0)

	flags = flags.Unset(15)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags16 bit at index 15")
	}

	flags = ^bitutil.Flags16(0)

	flags = flags.Unset15()
	if flags&mask != 0 {
		t.Error("failed .Unset15() Flags16 bit at index 15")
	}
}

func TestFlags32Get(t *testing.T) {
	var mask, flags bitutil.Flags32

	mask = bitutil.Flags32(1) << 0

	flags = 0

	flags |= mask
	if !flags.Get(0) {
		t.Error("failed .Get() set Flags32 bit at index 0")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get(0) {
		t.Error("failed .Get() unset Flags32 bit at index 0")
	}

	flags = 0

	flags |= mask
	if !flags.Get0() {
		t.Error("failed .Get0() set Flags32 bit at index 0")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get0() {
		t.Error("failed .Get0() unset Flags32 bit at index 0")
	}

	mask = bitutil.Flags32(1) << 1

	flags = 0

	flags |= mask
	if !flags.Get(1) {
		t.Error("failed .Get() set Flags32 bit at index 1")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get(1) {
		t.Error("failed .Get() unset Flags32 bit at index 1")
	}

	flags = 0

	flags |= mask
	if !flags.Get1() {
		t.Error("failed .Get1() set Flags32 bit at index 1")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get1() {
		t.Error("failed .Get1() unset Flags32 bit at index 1")
	}

	mask = bitutil.Flags32(1) << 2

	flags = 0

	flags |= mask
	if !flags.Get(2) {
		t.Error("failed .Get() set Flags32 bit at index 2")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get(2) {
		t.Error("failed .Get() unset Flags32 bit at index 2")
	}

	flags = 0

	flags |= mask
	if !flags.Get2() {
		t.Error("failed .Get2() set Flags32 bit at index 2")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get2() {
		t.Error("failed .Get2() unset Flags32 bit at index 2")
	}

	mask = bitutil.Flags32(1) << 3

	flags = 0

	flags |= mask
	if !flags.Get(3) {
		t.Error("failed .Get() set Flags32 bit at index 3")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get(3) {
		t.Error("failed .Get() unset Flags32 bit at index 3")
	}

	flags = 0

	flags |= mask
	if !flags.Get3() {
		t.Error("failed .Get3() set Flags32 bit at index 3")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get3() {
		t.Error("failed .Get3() unset Flags32 bit at index 3")
	}

	mask = bitutil.Flags32(1) << 4

	flags = 0

	flags |= mask
	if !flags.Get(4) {
		t.Error("failed .Get() set Flags32 bit at index 4")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get(4) {
		t.Error("failed .Get() unset Flags32 bit at index 4")
	}

	flags = 0

	flags |= mask
	if !flags.Get4() {
		t.Error("failed .Get4() set Flags32 bit at index 4")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get4() {
		t.Error("failed .Get4() unset Flags32 bit at index 4")
	}

	mask = bitutil.Flags32(1) << 5

	flags = 0

	flags |= mask
	if !flags.Get(5) {
		t.Error("failed .Get() set Flags32 bit at index 5")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get(5) {
		t.Error("failed .Get() unset Flags32 bit at index 5")
	}

	flags = 0

	flags |= mask
	if !flags.Get5() {
		t.Error("failed .Get5() set Flags32 bit at index 5")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get5() {
		t.Error("failed .Get5() unset Flags32 bit at index 5")
	}

	mask = bitutil.Flags32(1) << 6

	flags = 0

	flags |= mask
	if !flags.Get(6) {
		t.Error("failed .Get() set Flags32 bit at index 6")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get(6) {
		t.Error("failed .Get() unset Flags32 bit at index 6")
	}

	flags = 0

	flags |= mask
	if !flags.Get6() {
		t.Error("failed .Get6() set Flags32 bit at index 6")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get6() {
		t.Error("failed .Get6() unset Flags32 bit at index 6")
	}

	mask = bitutil.Flags32(1) << 7

	flags = 0

	flags |= mask
	if !flags.Get(7) {
		t.Error("failed .Get() set Flags32 bit at index 7")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get(7) {
		t.Error("failed .Get() unset Flags32 bit at index 7")
	}

	flags = 0

	flags |= mask
	if !flags.Get7() {
		t.Error("failed .Get7() set Flags32 bit at index 7")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get7() {
		t.Error("failed .Get7() unset Flags32 bit at index 7")
	}

	mask = bitutil.Flags32(1) << 8

	flags = 0

	flags |= mask
	if !flags.Get(8) {
		t.Error("failed .Get() set Flags32 bit at index 8")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get(8) {
		t.Error("failed .Get() unset Flags32 bit at index 8")
	}

	flags = 0

	flags |= mask
	if !flags.Get8() {
		t.Error("failed .Get8() set Flags32 bit at index 8")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get8() {
		t.Error("failed .Get8() unset Flags32 bit at index 8")
	}

	mask = bitutil.Flags32(1) << 9

	flags = 0

	flags |= mask
	if !flags.Get(9) {
		t.Error("failed .Get() set Flags32 bit at index 9")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get(9) {
		t.Error("failed .Get() unset Flags32 bit at index 9")
	}

	flags = 0

	flags |= mask
	if !flags.Get9() {
		t.Error("failed .Get9() set Flags32 bit at index 9")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get9() {
		t.Error("failed .Get9() unset Flags32 bit at index 9")
	}

	mask = bitutil.Flags32(1) << 10

	flags = 0

	flags |= mask
	if !flags.Get(10) {
		t.Error("failed .Get() set Flags32 bit at index 10")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get(10) {
		t.Error("failed .Get() unset Flags32 bit at index 10")
	}

	flags = 0

	flags |= mask
	if !flags.Get10() {
		t.Error("failed .Get10() set Flags32 bit at index 10")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get10() {
		t.Error("failed .Get10() unset Flags32 bit at index 10")
	}

	mask = bitutil.Flags32(1) << 11

	flags = 0

	flags |= mask
	if !flags.Get(11) {
		t.Error("failed .Get() set Flags32 bit at index 11")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get(11) {
		t.Error("failed .Get() unset Flags32 bit at index 11")
	}

	flags = 0

	flags |= mask
	if !flags.Get11() {
		t.Error("failed .Get11() set Flags32 bit at index 11")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get11() {
		t.Error("failed .Get11() unset Flags32 bit at index 11")
	}

	mask = bitutil.Flags32(1) << 12

	flags = 0

	flags |= mask
	if !flags.Get(12) {
		t.Error("failed .Get() set Flags32 bit at index 12")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get(12) {
		t.Error("failed .Get() unset Flags32 bit at index 12")
	}

	flags = 0

	flags |= mask
	if !flags.Get12() {
		t.Error("failed .Get12() set Flags32 bit at index 12")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get12() {
		t.Error("failed .Get12() unset Flags32 bit at index 12")
	}

	mask = bitutil.Flags32(1) << 13

	flags = 0

	flags |= mask
	if !flags.Get(13) {
		t.Error("failed .Get() set Flags32 bit at index 13")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get(13) {
		t.Error("failed .Get() unset Flags32 bit at index 13")
	}

	flags = 0

	flags |= mask
	if !flags.Get13() {
		t.Error("failed .Get13() set Flags32 bit at index 13")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get13() {
		t.Error("failed .Get13() unset Flags32 bit at index 13")
	}

	mask = bitutil.Flags32(1) << 14

	flags = 0

	flags |= mask
	if !flags.Get(14) {
		t.Error("failed .Get() set Flags32 bit at index 14")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get(14) {
		t.Error("failed .Get() unset Flags32 bit at index 14")
	}

	flags = 0

	flags |= mask
	if !flags.Get14() {
		t.Error("failed .Get14() set Flags32 bit at index 14")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get14() {
		t.Error("failed .Get14() unset Flags32 bit at index 14")
	}

	mask = bitutil.Flags32(1) << 15

	flags = 0

	flags |= mask
	if !flags.Get(15) {
		t.Error("failed .Get() set Flags32 bit at index 15")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get(15) {
		t.Error("failed .Get() unset Flags32 bit at index 15")
	}

	flags = 0

	flags |= mask
	if !flags.Get15() {
		t.Error("failed .Get15() set Flags32 bit at index 15")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get15() {
		t.Error("failed .Get15() unset Flags32 bit at index 15")
	}

	mask = bitutil.Flags32(1) << 16

	flags = 0

	flags |= mask
	if !flags.Get(16) {
		t.Error("failed .Get() set Flags32 bit at index 16")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get(16) {
		t.Error("failed .Get() unset Flags32 bit at index 16")
	}

	flags = 0

	flags |= mask
	if !flags.Get16() {
		t.Error("failed .Get16() set Flags32 bit at index 16")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get16() {
		t.Error("failed .Get16() unset Flags32 bit at index 16")
	}

	mask = bitutil.Flags32(1) << 17

	flags = 0

	flags |= mask
	if !flags.Get(17) {
		t.Error("failed .Get() set Flags32 bit at index 17")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get(17) {
		t.Error("failed .Get() unset Flags32 bit at index 17")
	}

	flags = 0

	flags |= mask
	if !flags.Get17() {
		t.Error("failed .Get17() set Flags32 bit at index 17")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get17() {
		t.Error("failed .Get17() unset Flags32 bit at index 17")
	}

	mask = bitutil.Flags32(1) << 18

	flags = 0

	flags |= mask
	if !flags.Get(18) {
		t.Error("failed .Get() set Flags32 bit at index 18")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get(18) {
		t.Error("failed .Get() unset Flags32 bit at index 18")
	}

	flags = 0

	flags |= mask
	if !flags.Get18() {
		t.Error("failed .Get18() set Flags32 bit at index 18")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get18() {
		t.Error("failed .Get18() unset Flags32 bit at index 18")
	}

	mask = bitutil.Flags32(1) << 19

	flags = 0

	flags |= mask
	if !flags.Get(19) {
		t.Error("failed .Get() set Flags32 bit at index 19")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get(19) {
		t.Error("failed .Get() unset Flags32 bit at index 19")
	}

	flags = 0

	flags |= mask
	if !flags.Get19() {
		t.Error("failed .Get19() set Flags32 bit at index 19")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get19() {
		t.Error("failed .Get19() unset Flags32 bit at index 19")
	}

	mask = bitutil.Flags32(1) << 20

	flags = 0

	flags |= mask
	if !flags.Get(20) {
		t.Error("failed .Get() set Flags32 bit at index 20")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get(20) {
		t.Error("failed .Get() unset Flags32 bit at index 20")
	}

	flags = 0

	flags |= mask
	if !flags.Get20() {
		t.Error("failed .Get20() set Flags32 bit at index 20")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get20() {
		t.Error("failed .Get20() unset Flags32 bit at index 20")
	}

	mask = bitutil.Flags32(1) << 21

	flags = 0

	flags |= mask
	if !flags.Get(21) {
		t.Error("failed .Get() set Flags32 bit at index 21")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get(21) {
		t.Error("failed .Get() unset Flags32 bit at index 21")
	}

	flags = 0

	flags |= mask
	if !flags.Get21() {
		t.Error("failed .Get21() set Flags32 bit at index 21")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get21() {
		t.Error("failed .Get21() unset Flags32 bit at index 21")
	}

	mask = bitutil.Flags32(1) << 22

	flags = 0

	flags |= mask
	if !flags.Get(22) {
		t.Error("failed .Get() set Flags32 bit at index 22")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get(22) {
		t.Error("failed .Get() unset Flags32 bit at index 22")
	}

	flags = 0

	flags |= mask
	if !flags.Get22() {
		t.Error("failed .Get22() set Flags32 bit at index 22")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get22() {
		t.Error("failed .Get22() unset Flags32 bit at index 22")
	}

	mask = bitutil.Flags32(1) << 23

	flags = 0

	flags |= mask
	if !flags.Get(23) {
		t.Error("failed .Get() set Flags32 bit at index 23")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get(23) {
		t.Error("failed .Get() unset Flags32 bit at index 23")
	}

	flags = 0

	flags |= mask
	if !flags.Get23() {
		t.Error("failed .Get23() set Flags32 bit at index 23")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get23() {
		t.Error("failed .Get23() unset Flags32 bit at index 23")
	}

	mask = bitutil.Flags32(1) << 24

	flags = 0

	flags |= mask
	if !flags.Get(24) {
		t.Error("failed .Get() set Flags32 bit at index 24")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get(24) {
		t.Error("failed .Get() unset Flags32 bit at index 24")
	}

	flags = 0

	flags |= mask
	if !flags.Get24() {
		t.Error("failed .Get24() set Flags32 bit at index 24")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get24() {
		t.Error("failed .Get24() unset Flags32 bit at index 24")
	}

	mask = bitutil.Flags32(1) << 25

	flags = 0

	flags |= mask
	if !flags.Get(25) {
		t.Error("failed .Get() set Flags32 bit at index 25")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get(25) {
		t.Error("failed .Get() unset Flags32 bit at index 25")
	}

	flags = 0

	flags |= mask
	if !flags.Get25() {
		t.Error("failed .Get25() set Flags32 bit at index 25")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get25() {
		t.Error("failed .Get25() unset Flags32 bit at index 25")
	}

	mask = bitutil.Flags32(1) << 26

	flags = 0

	flags |= mask
	if !flags.Get(26) {
		t.Error("failed .Get() set Flags32 bit at index 26")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get(26) {
		t.Error("failed .Get() unset Flags32 bit at index 26")
	}

	flags = 0

	flags |= mask
	if !flags.Get26() {
		t.Error("failed .Get26() set Flags32 bit at index 26")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get26() {
		t.Error("failed .Get26() unset Flags32 bit at index 26")
	}

	mask = bitutil.Flags32(1) << 27

	flags = 0

	flags |= mask
	if !flags.Get(27) {
		t.Error("failed .Get() set Flags32 bit at index 27")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get(27) {
		t.Error("failed .Get() unset Flags32 bit at index 27")
	}

	flags = 0

	flags |= mask
	if !flags.Get27() {
		t.Error("failed .Get27() set Flags32 bit at index 27")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get27() {
		t.Error("failed .Get27() unset Flags32 bit at index 27")
	}

	mask = bitutil.Flags32(1) << 28

	flags = 0

	flags |= mask
	if !flags.Get(28) {
		t.Error("failed .Get() set Flags32 bit at index 28")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get(28) {
		t.Error("failed .Get() unset Flags32 bit at index 28")
	}

	flags = 0

	flags |= mask
	if !flags.Get28() {
		t.Error("failed .Get28() set Flags32 bit at index 28")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get28() {
		t.Error("failed .Get28() unset Flags32 bit at index 28")
	}

	mask = bitutil.Flags32(1) << 29

	flags = 0

	flags |= mask
	if !flags.Get(29) {
		t.Error("failed .Get() set Flags32 bit at index 29")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get(29) {
		t.Error("failed .Get() unset Flags32 bit at index 29")
	}

	flags = 0

	flags |= mask
	if !flags.Get29() {
		t.Error("failed .Get29() set Flags32 bit at index 29")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get29() {
		t.Error("failed .Get29() unset Flags32 bit at index 29")
	}

	mask = bitutil.Flags32(1) << 30

	flags = 0

	flags |= mask
	if !flags.Get(30) {
		t.Error("failed .Get() set Flags32 bit at index 30")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get(30) {
		t.Error("failed .Get() unset Flags32 bit at index 30")
	}

	flags = 0

	flags |= mask
	if !flags.Get30() {
		t.Error("failed .Get30() set Flags32 bit at index 30")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get30() {
		t.Error("failed .Get30() unset Flags32 bit at index 30")
	}

	mask = bitutil.Flags32(1) << 31

	flags = 0

	flags |= mask
	if !flags.Get(31) {
		t.Error("failed .Get() set Flags32 bit at index 31")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get(31) {
		t.Error("failed .Get() unset Flags32 bit at index 31")
	}

	flags = 0

	flags |= mask
	if !flags.Get31() {
		t.Error("failed .Get31() set Flags32 bit at index 31")
	}

	flags = ^bitutil.Flags32(0)

	flags &= ^mask
	if flags.Get31() {
		t.Error("failed .Get31() unset Flags32 bit at index 31")
	}
}

func TestFlags32Set(t *testing.T) {
	var mask, flags bitutil.Flags32

	mask = bitutil.Flags32(1) << 0

	flags = 0

	flags = flags.Set(0)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags32 bit at index 0")
	}

	flags = 0

	flags = flags.Set0()
	if flags&mask == 0 {
		t.Error("failed .Set0() Flags32 bit at index 0")
	}

	mask = bitutil.Flags32(1) << 1

	flags = 0

	flags = flags.Set(1)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags32 bit at index 1")
	}

	flags = 0

	flags = flags.Set1()
	if flags&mask == 0 {
		t.Error("failed .Set1() Flags32 bit at index 1")
	}

	mask = bitutil.Flags32(1) << 2

	flags = 0

	flags = flags.Set(2)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags32 bit at index 2")
	}

	flags = 0

	flags = flags.Set2()
	if flags&mask == 0 {
		t.Error("failed .Set2() Flags32 bit at index 2")
	}

	mask = bitutil.Flags32(1) << 3

	flags = 0

	flags = flags.Set(3)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags32 bit at index 3")
	}

	flags = 0

	flags = flags.Set3()
	if flags&mask == 0 {
		t.Error("failed .Set3() Flags32 bit at index 3")
	}

	mask = bitutil.Flags32(1) << 4

	flags = 0

	flags = flags.Set(4)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags32 bit at index 4")
	}

	flags = 0

	flags = flags.Set4()
	if flags&mask == 0 {
		t.Error("failed .Set4() Flags32 bit at index 4")
	}

	mask = bitutil.Flags32(1) << 5

	flags = 0

	flags = flags.Set(5)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags32 bit at index 5")
	}

	flags = 0

	flags = flags.Set5()
	if flags&mask == 0 {
		t.Error("failed .Set5() Flags32 bit at index 5")
	}

	mask = bitutil.Flags32(1) << 6

	flags = 0

	flags = flags.Set(6)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags32 bit at index 6")
	}

	flags = 0

	flags = flags.Set6()
	if flags&mask == 0 {
		t.Error("failed .Set6() Flags32 bit at index 6")
	}

	mask = bitutil.Flags32(1) << 7

	flags = 0

	flags = flags.Set(7)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags32 bit at index 7")
	}

	flags = 0

	flags = flags.Set7()
	if flags&mask == 0 {
		t.Error("failed .Set7() Flags32 bit at index 7")
	}

	mask = bitutil.Flags32(1) << 8

	flags = 0

	flags = flags.Set(8)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags32 bit at index 8")
	}

	flags = 0

	flags = flags.Set8()
	if flags&mask == 0 {
		t.Error("failed .Set8() Flags32 bit at index 8")
	}

	mask = bitutil.Flags32(1) << 9

	flags = 0

	flags = flags.Set(9)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags32 bit at index 9")
	}

	flags = 0

	flags = flags.Set9()
	if flags&mask == 0 {
		t.Error("failed .Set9() Flags32 bit at index 9")
	}

	mask = bitutil.Flags32(1) << 10

	flags = 0

	flags = flags.Set(10)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags32 bit at index 10")
	}

	flags = 0

	flags = flags.Set10()
	if flags&mask == 0 {
		t.Error("failed .Set10() Flags32 bit at index 10")
	}

	mask = bitutil.Flags32(1) << 11

	flags = 0

	flags = flags.Set(11)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags32 bit at index 11")
	}

	flags = 0

	flags = flags.Set11()
	if flags&mask == 0 {
		t.Error("failed .Set11() Flags32 bit at index 11")
	}

	mask = bitutil.Flags32(1) << 12

	flags = 0

	flags = flags.Set(12)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags32 bit at index 12")
	}

	flags = 0

	flags = flags.Set12()
	if flags&mask == 0 {
		t.Error("failed .Set12() Flags32 bit at index 12")
	}

	mask = bitutil.Flags32(1) << 13

	flags = 0

	flags = flags.Set(13)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags32 bit at index 13")
	}

	flags = 0

	flags = flags.Set13()
	if flags&mask == 0 {
		t.Error("failed .Set13() Flags32 bit at index 13")
	}

	mask = bitutil.Flags32(1) << 14

	flags = 0

	flags = flags.Set(14)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags32 bit at index 14")
	}

	flags = 0

	flags = flags.Set14()
	if flags&mask == 0 {
		t.Error("failed .Set14() Flags32 bit at index 14")
	}

	mask = bitutil.Flags32(1) << 15

	flags = 0

	flags = flags.Set(15)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags32 bit at index 15")
	}

	flags = 0

	flags = flags.Set15()
	if flags&mask == 0 {
		t.Error("failed .Set15() Flags32 bit at index 15")
	}

	mask = bitutil.Flags32(1) << 16

	flags = 0

	flags = flags.Set(16)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags32 bit at index 16")
	}

	flags = 0

	flags = flags.Set16()
	if flags&mask == 0 {
		t.Error("failed .Set16() Flags32 bit at index 16")
	}

	mask = bitutil.Flags32(1) << 17

	flags = 0

	flags = flags.Set(17)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags32 bit at index 17")
	}

	flags = 0

	flags = flags.Set17()
	if flags&mask == 0 {
		t.Error("failed .Set17() Flags32 bit at index 17")
	}

	mask = bitutil.Flags32(1) << 18

	flags = 0

	flags = flags.Set(18)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags32 bit at index 18")
	}

	flags = 0

	flags = flags.Set18()
	if flags&mask == 0 {
		t.Error("failed .Set18() Flags32 bit at index 18")
	}

	mask = bitutil.Flags32(1) << 19

	flags = 0

	flags = flags.Set(19)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags32 bit at index 19")
	}

	flags = 0

	flags = flags.Set19()
	if flags&mask == 0 {
		t.Error("failed .Set19() Flags32 bit at index 19")
	}

	mask = bitutil.Flags32(1) << 20

	flags = 0

	flags = flags.Set(20)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags32 bit at index 20")
	}

	flags = 0

	flags = flags.Set20()
	if flags&mask == 0 {
		t.Error("failed .Set20() Flags32 bit at index 20")
	}

	mask = bitutil.Flags32(1) << 21

	flags = 0

	flags = flags.Set(21)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags32 bit at index 21")
	}

	flags = 0

	flags = flags.Set21()
	if flags&mask == 0 {
		t.Error("failed .Set21() Flags32 bit at index 21")
	}

	mask = bitutil.Flags32(1) << 22

	flags = 0

	flags = flags.Set(22)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags32 bit at index 22")
	}

	flags = 0

	flags = flags.Set22()
	if flags&mask == 0 {
		t.Error("failed .Set22() Flags32 bit at index 22")
	}

	mask = bitutil.Flags32(1) << 23

	flags = 0

	flags = flags.Set(23)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags32 bit at index 23")
	}

	flags = 0

	flags = flags.Set23()
	if flags&mask == 0 {
		t.Error("failed .Set23() Flags32 bit at index 23")
	}

	mask = bitutil.Flags32(1) << 24

	flags = 0

	flags = flags.Set(24)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags32 bit at index 24")
	}

	flags = 0

	flags = flags.Set24()
	if flags&mask == 0 {
		t.Error("failed .Set24() Flags32 bit at index 24")
	}

	mask = bitutil.Flags32(1) << 25

	flags = 0

	flags = flags.Set(25)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags32 bit at index 25")
	}

	flags = 0

	flags = flags.Set25()
	if flags&mask == 0 {
		t.Error("failed .Set25() Flags32 bit at index 25")
	}

	mask = bitutil.Flags32(1) << 26

	flags = 0

	flags = flags.Set(26)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags32 bit at index 26")
	}

	flags = 0

	flags = flags.Set26()
	if flags&mask == 0 {
		t.Error("failed .Set26() Flags32 bit at index 26")
	}

	mask = bitutil.Flags32(1) << 27

	flags = 0

	flags = flags.Set(27)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags32 bit at index 27")
	}

	flags = 0

	flags = flags.Set27()
	if flags&mask == 0 {
		t.Error("failed .Set27() Flags32 bit at index 27")
	}

	mask = bitutil.Flags32(1) << 28

	flags = 0

	flags = flags.Set(28)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags32 bit at index 28")
	}

	flags = 0

	flags = flags.Set28()
	if flags&mask == 0 {
		t.Error("failed .Set28() Flags32 bit at index 28")
	}

	mask = bitutil.Flags32(1) << 29

	flags = 0

	flags = flags.Set(29)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags32 bit at index 29")
	}

	flags = 0

	flags = flags.Set29()
	if flags&mask == 0 {
		t.Error("failed .Set29() Flags32 bit at index 29")
	}

	mask = bitutil.Flags32(1) << 30

	flags = 0

	flags = flags.Set(30)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags32 bit at index 30")
	}

	flags = 0

	flags = flags.Set30()
	if flags&mask == 0 {
		t.Error("failed .Set30() Flags32 bit at index 30")
	}

	mask = bitutil.Flags32(1) << 31

	flags = 0

	flags = flags.Set(31)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags32 bit at index 31")
	}

	flags = 0

	flags = flags.Set31()
	if flags&mask == 0 {
		t.Error("failed .Set31() Flags32 bit at index 31")
	}
}

func TestFlags32Unset(t *testing.T) {
	var mask, flags bitutil.Flags32

	mask = bitutil.Flags32(1) << 0

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset(0)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags32 bit at index 0")
	}

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset0()
	if flags&mask != 0 {
		t.Error("failed .Unset0() Flags32 bit at index 0")
	}

	mask = bitutil.Flags32(1) << 1

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset(1)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags32 bit at index 1")
	}

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset1()
	if flags&mask != 0 {
		t.Error("failed .Unset1() Flags32 bit at index 1")
	}

	mask = bitutil.Flags32(1) << 2

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset(2)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags32 bit at index 2")
	}

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset2()
	if flags&mask != 0 {
		t.Error("failed .Unset2() Flags32 bit at index 2")
	}

	mask = bitutil.Flags32(1) << 3

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset(3)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags32 bit at index 3")
	}

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset3()
	if flags&mask != 0 {
		t.Error("failed .Unset3() Flags32 bit at index 3")
	}

	mask = bitutil.Flags32(1) << 4

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset(4)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags32 bit at index 4")
	}

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset4()
	if flags&mask != 0 {
		t.Error("failed .Unset4() Flags32 bit at index 4")
	}

	mask = bitutil.Flags32(1) << 5

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset(5)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags32 bit at index 5")
	}

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset5()
	if flags&mask != 0 {
		t.Error("failed .Unset5() Flags32 bit at index 5")
	}

	mask = bitutil.Flags32(1) << 6

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset(6)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags32 bit at index 6")
	}

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset6()
	if flags&mask != 0 {
		t.Error("failed .Unset6() Flags32 bit at index 6")
	}

	mask = bitutil.Flags32(1) << 7

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset(7)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags32 bit at index 7")
	}

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset7()
	if flags&mask != 0 {
		t.Error("failed .Unset7() Flags32 bit at index 7")
	}

	mask = bitutil.Flags32(1) << 8

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset(8)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags32 bit at index 8")
	}

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset8()
	if flags&mask != 0 {
		t.Error("failed .Unset8() Flags32 bit at index 8")
	}

	mask = bitutil.Flags32(1) << 9

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset(9)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags32 bit at index 9")
	}

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset9()
	if flags&mask != 0 {
		t.Error("failed .Unset9() Flags32 bit at index 9")
	}

	mask = bitutil.Flags32(1) << 10

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset(10)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags32 bit at index 10")
	}

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset10()
	if flags&mask != 0 {
		t.Error("failed .Unset10() Flags32 bit at index 10")
	}

	mask = bitutil.Flags32(1) << 11

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset(11)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags32 bit at index 11")
	}

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset11()
	if flags&mask != 0 {
		t.Error("failed .Unset11() Flags32 bit at index 11")
	}

	mask = bitutil.Flags32(1) << 12

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset(12)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags32 bit at index 12")
	}

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset12()
	if flags&mask != 0 {
		t.Error("failed .Unset12() Flags32 bit at index 12")
	}

	mask = bitutil.Flags32(1) << 13

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset(13)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags32 bit at index 13")
	}

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset13()
	if flags&mask != 0 {
		t.Error("failed .Unset13() Flags32 bit at index 13")
	}

	mask = bitutil.Flags32(1) << 14

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset(14)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags32 bit at index 14")
	}

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset14()
	if flags&mask != 0 {
		t.Error("failed .Unset14() Flags32 bit at index 14")
	}

	mask = bitutil.Flags32(1) << 15

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset(15)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags32 bit at index 15")
	}

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset15()
	if flags&mask != 0 {
		t.Error("failed .Unset15() Flags32 bit at index 15")
	}

	mask = bitutil.Flags32(1) << 16

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset(16)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags32 bit at index 16")
	}

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset16()
	if flags&mask != 0 {
		t.Error("failed .Unset16() Flags32 bit at index 16")
	}

	mask = bitutil.Flags32(1) << 17

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset(17)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags32 bit at index 17")
	}

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset17()
	if flags&mask != 0 {
		t.Error("failed .Unset17() Flags32 bit at index 17")
	}

	mask = bitutil.Flags32(1) << 18

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset(18)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags32 bit at index 18")
	}

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset18()
	if flags&mask != 0 {
		t.Error("failed .Unset18() Flags32 bit at index 18")
	}

	mask = bitutil.Flags32(1) << 19

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset(19)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags32 bit at index 19")
	}

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset19()
	if flags&mask != 0 {
		t.Error("failed .Unset19() Flags32 bit at index 19")
	}

	mask = bitutil.Flags32(1) << 20

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset(20)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags32 bit at index 20")
	}

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset20()
	if flags&mask != 0 {
		t.Error("failed .Unset20() Flags32 bit at index 20")
	}

	mask = bitutil.Flags32(1) << 21

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset(21)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags32 bit at index 21")
	}

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset21()
	if flags&mask != 0 {
		t.Error("failed .Unset21() Flags32 bit at index 21")
	}

	mask = bitutil.Flags32(1) << 22

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset(22)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags32 bit at index 22")
	}

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset22()
	if flags&mask != 0 {
		t.Error("failed .Unset22() Flags32 bit at index 22")
	}

	mask = bitutil.Flags32(1) << 23

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset(23)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags32 bit at index 23")
	}

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset23()
	if flags&mask != 0 {
		t.Error("failed .Unset23() Flags32 bit at index 23")
	}

	mask = bitutil.Flags32(1) << 24

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset(24)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags32 bit at index 24")
	}

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset24()
	if flags&mask != 0 {
		t.Error("failed .Unset24() Flags32 bit at index 24")
	}

	mask = bitutil.Flags32(1) << 25

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset(25)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags32 bit at index 25")
	}

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset25()
	if flags&mask != 0 {
		t.Error("failed .Unset25() Flags32 bit at index 25")
	}

	mask = bitutil.Flags32(1) << 26

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset(26)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags32 bit at index 26")
	}

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset26()
	if flags&mask != 0 {
		t.Error("failed .Unset26() Flags32 bit at index 26")
	}

	mask = bitutil.Flags32(1) << 27

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset(27)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags32 bit at index 27")
	}

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset27()
	if flags&mask != 0 {
		t.Error("failed .Unset27() Flags32 bit at index 27")
	}

	mask = bitutil.Flags32(1) << 28

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset(28)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags32 bit at index 28")
	}

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset28()
	if flags&mask != 0 {
		t.Error("failed .Unset28() Flags32 bit at index 28")
	}

	mask = bitutil.Flags32(1) << 29

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset(29)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags32 bit at index 29")
	}

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset29()
	if flags&mask != 0 {
		t.Error("failed .Unset29() Flags32 bit at index 29")
	}

	mask = bitutil.Flags32(1) << 30

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset(30)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags32 bit at index 30")
	}

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset30()
	if flags&mask != 0 {
		t.Error("failed .Unset30() Flags32 bit at index 30")
	}

	mask = bitutil.Flags32(1) << 31

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset(31)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags32 bit at index 31")
	}

	flags = ^bitutil.Flags32(0)

	flags = flags.Unset31()
	if flags&mask != 0 {
		t.Error("failed .Unset31() Flags32 bit at index 31")
	}
}

func TestFlags64Get(t *testing.T) {
	var mask, flags bitutil.Flags64

	mask = bitutil.Flags64(1) << 0

	flags = 0

	flags |= mask
	if !flags.Get(0) {
		t.Error("failed .Get() set Flags64 bit at index 0")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(0) {
		t.Error("failed .Get() unset Flags64 bit at index 0")
	}

	flags = 0

	flags |= mask
	if !flags.Get0() {
		t.Error("failed .Get0() set Flags64 bit at index 0")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get0() {
		t.Error("failed .Get0() unset Flags64 bit at index 0")
	}

	mask = bitutil.Flags64(1) << 1

	flags = 0

	flags |= mask
	if !flags.Get(1) {
		t.Error("failed .Get() set Flags64 bit at index 1")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(1) {
		t.Error("failed .Get() unset Flags64 bit at index 1")
	}

	flags = 0

	flags |= mask
	if !flags.Get1() {
		t.Error("failed .Get1() set Flags64 bit at index 1")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get1() {
		t.Error("failed .Get1() unset Flags64 bit at index 1")
	}

	mask = bitutil.Flags64(1) << 2

	flags = 0

	flags |= mask
	if !flags.Get(2) {
		t.Error("failed .Get() set Flags64 bit at index 2")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(2) {
		t.Error("failed .Get() unset Flags64 bit at index 2")
	}

	flags = 0

	flags |= mask
	if !flags.Get2() {
		t.Error("failed .Get2() set Flags64 bit at index 2")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get2() {
		t.Error("failed .Get2() unset Flags64 bit at index 2")
	}

	mask = bitutil.Flags64(1) << 3

	flags = 0

	flags |= mask
	if !flags.Get(3) {
		t.Error("failed .Get() set Flags64 bit at index 3")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(3) {
		t.Error("failed .Get() unset Flags64 bit at index 3")
	}

	flags = 0

	flags |= mask
	if !flags.Get3() {
		t.Error("failed .Get3() set Flags64 bit at index 3")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get3() {
		t.Error("failed .Get3() unset Flags64 bit at index 3")
	}

	mask = bitutil.Flags64(1) << 4

	flags = 0

	flags |= mask
	if !flags.Get(4) {
		t.Error("failed .Get() set Flags64 bit at index 4")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(4) {
		t.Error("failed .Get() unset Flags64 bit at index 4")
	}

	flags = 0

	flags |= mask
	if !flags.Get4() {
		t.Error("failed .Get4() set Flags64 bit at index 4")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get4() {
		t.Error("failed .Get4() unset Flags64 bit at index 4")
	}

	mask = bitutil.Flags64(1) << 5

	flags = 0

	flags |= mask
	if !flags.Get(5) {
		t.Error("failed .Get() set Flags64 bit at index 5")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(5) {
		t.Error("failed .Get() unset Flags64 bit at index 5")
	}

	flags = 0

	flags |= mask
	if !flags.Get5() {
		t.Error("failed .Get5() set Flags64 bit at index 5")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get5() {
		t.Error("failed .Get5() unset Flags64 bit at index 5")
	}

	mask = bitutil.Flags64(1) << 6

	flags = 0

	flags |= mask
	if !flags.Get(6) {
		t.Error("failed .Get() set Flags64 bit at index 6")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(6) {
		t.Error("failed .Get() unset Flags64 bit at index 6")
	}

	flags = 0

	flags |= mask
	if !flags.Get6() {
		t.Error("failed .Get6() set Flags64 bit at index 6")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get6() {
		t.Error("failed .Get6() unset Flags64 bit at index 6")
	}

	mask = bitutil.Flags64(1) << 7

	flags = 0

	flags |= mask
	if !flags.Get(7) {
		t.Error("failed .Get() set Flags64 bit at index 7")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(7) {
		t.Error("failed .Get() unset Flags64 bit at index 7")
	}

	flags = 0

	flags |= mask
	if !flags.Get7() {
		t.Error("failed .Get7() set Flags64 bit at index 7")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get7() {
		t.Error("failed .Get7() unset Flags64 bit at index 7")
	}

	mask = bitutil.Flags64(1) << 8

	flags = 0

	flags |= mask
	if !flags.Get(8) {
		t.Error("failed .Get() set Flags64 bit at index 8")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(8) {
		t.Error("failed .Get() unset Flags64 bit at index 8")
	}

	flags = 0

	flags |= mask
	if !flags.Get8() {
		t.Error("failed .Get8() set Flags64 bit at index 8")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get8() {
		t.Error("failed .Get8() unset Flags64 bit at index 8")
	}

	mask = bitutil.Flags64(1) << 9

	flags = 0

	flags |= mask
	if !flags.Get(9) {
		t.Error("failed .Get() set Flags64 bit at index 9")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(9) {
		t.Error("failed .Get() unset Flags64 bit at index 9")
	}

	flags = 0

	flags |= mask
	if !flags.Get9() {
		t.Error("failed .Get9() set Flags64 bit at index 9")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get9() {
		t.Error("failed .Get9() unset Flags64 bit at index 9")
	}

	mask = bitutil.Flags64(1) << 10

	flags = 0

	flags |= mask
	if !flags.Get(10) {
		t.Error("failed .Get() set Flags64 bit at index 10")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(10) {
		t.Error("failed .Get() unset Flags64 bit at index 10")
	}

	flags = 0

	flags |= mask
	if !flags.Get10() {
		t.Error("failed .Get10() set Flags64 bit at index 10")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get10() {
		t.Error("failed .Get10() unset Flags64 bit at index 10")
	}

	mask = bitutil.Flags64(1) << 11

	flags = 0

	flags |= mask
	if !flags.Get(11) {
		t.Error("failed .Get() set Flags64 bit at index 11")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(11) {
		t.Error("failed .Get() unset Flags64 bit at index 11")
	}

	flags = 0

	flags |= mask
	if !flags.Get11() {
		t.Error("failed .Get11() set Flags64 bit at index 11")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get11() {
		t.Error("failed .Get11() unset Flags64 bit at index 11")
	}

	mask = bitutil.Flags64(1) << 12

	flags = 0

	flags |= mask
	if !flags.Get(12) {
		t.Error("failed .Get() set Flags64 bit at index 12")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(12) {
		t.Error("failed .Get() unset Flags64 bit at index 12")
	}

	flags = 0

	flags |= mask
	if !flags.Get12() {
		t.Error("failed .Get12() set Flags64 bit at index 12")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get12() {
		t.Error("failed .Get12() unset Flags64 bit at index 12")
	}

	mask = bitutil.Flags64(1) << 13

	flags = 0

	flags |= mask
	if !flags.Get(13) {
		t.Error("failed .Get() set Flags64 bit at index 13")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(13) {
		t.Error("failed .Get() unset Flags64 bit at index 13")
	}

	flags = 0

	flags |= mask
	if !flags.Get13() {
		t.Error("failed .Get13() set Flags64 bit at index 13")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get13() {
		t.Error("failed .Get13() unset Flags64 bit at index 13")
	}

	mask = bitutil.Flags64(1) << 14

	flags = 0

	flags |= mask
	if !flags.Get(14) {
		t.Error("failed .Get() set Flags64 bit at index 14")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(14) {
		t.Error("failed .Get() unset Flags64 bit at index 14")
	}

	flags = 0

	flags |= mask
	if !flags.Get14() {
		t.Error("failed .Get14() set Flags64 bit at index 14")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get14() {
		t.Error("failed .Get14() unset Flags64 bit at index 14")
	}

	mask = bitutil.Flags64(1) << 15

	flags = 0

	flags |= mask
	if !flags.Get(15) {
		t.Error("failed .Get() set Flags64 bit at index 15")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(15) {
		t.Error("failed .Get() unset Flags64 bit at index 15")
	}

	flags = 0

	flags |= mask
	if !flags.Get15() {
		t.Error("failed .Get15() set Flags64 bit at index 15")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get15() {
		t.Error("failed .Get15() unset Flags64 bit at index 15")
	}

	mask = bitutil.Flags64(1) << 16

	flags = 0

	flags |= mask
	if !flags.Get(16) {
		t.Error("failed .Get() set Flags64 bit at index 16")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(16) {
		t.Error("failed .Get() unset Flags64 bit at index 16")
	}

	flags = 0

	flags |= mask
	if !flags.Get16() {
		t.Error("failed .Get16() set Flags64 bit at index 16")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get16() {
		t.Error("failed .Get16() unset Flags64 bit at index 16")
	}

	mask = bitutil.Flags64(1) << 17

	flags = 0

	flags |= mask
	if !flags.Get(17) {
		t.Error("failed .Get() set Flags64 bit at index 17")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(17) {
		t.Error("failed .Get() unset Flags64 bit at index 17")
	}

	flags = 0

	flags |= mask
	if !flags.Get17() {
		t.Error("failed .Get17() set Flags64 bit at index 17")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get17() {
		t.Error("failed .Get17() unset Flags64 bit at index 17")
	}

	mask = bitutil.Flags64(1) << 18

	flags = 0

	flags |= mask
	if !flags.Get(18) {
		t.Error("failed .Get() set Flags64 bit at index 18")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(18) {
		t.Error("failed .Get() unset Flags64 bit at index 18")
	}

	flags = 0

	flags |= mask
	if !flags.Get18() {
		t.Error("failed .Get18() set Flags64 bit at index 18")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get18() {
		t.Error("failed .Get18() unset Flags64 bit at index 18")
	}

	mask = bitutil.Flags64(1) << 19

	flags = 0

	flags |= mask
	if !flags.Get(19) {
		t.Error("failed .Get() set Flags64 bit at index 19")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(19) {
		t.Error("failed .Get() unset Flags64 bit at index 19")
	}

	flags = 0

	flags |= mask
	if !flags.Get19() {
		t.Error("failed .Get19() set Flags64 bit at index 19")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get19() {
		t.Error("failed .Get19() unset Flags64 bit at index 19")
	}

	mask = bitutil.Flags64(1) << 20

	flags = 0

	flags |= mask
	if !flags.Get(20) {
		t.Error("failed .Get() set Flags64 bit at index 20")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(20) {
		t.Error("failed .Get() unset Flags64 bit at index 20")
	}

	flags = 0

	flags |= mask
	if !flags.Get20() {
		t.Error("failed .Get20() set Flags64 bit at index 20")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get20() {
		t.Error("failed .Get20() unset Flags64 bit at index 20")
	}

	mask = bitutil.Flags64(1) << 21

	flags = 0

	flags |= mask
	if !flags.Get(21) {
		t.Error("failed .Get() set Flags64 bit at index 21")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(21) {
		t.Error("failed .Get() unset Flags64 bit at index 21")
	}

	flags = 0

	flags |= mask
	if !flags.Get21() {
		t.Error("failed .Get21() set Flags64 bit at index 21")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get21() {
		t.Error("failed .Get21() unset Flags64 bit at index 21")
	}

	mask = bitutil.Flags64(1) << 22

	flags = 0

	flags |= mask
	if !flags.Get(22) {
		t.Error("failed .Get() set Flags64 bit at index 22")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(22) {
		t.Error("failed .Get() unset Flags64 bit at index 22")
	}

	flags = 0

	flags |= mask
	if !flags.Get22() {
		t.Error("failed .Get22() set Flags64 bit at index 22")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get22() {
		t.Error("failed .Get22() unset Flags64 bit at index 22")
	}

	mask = bitutil.Flags64(1) << 23

	flags = 0

	flags |= mask
	if !flags.Get(23) {
		t.Error("failed .Get() set Flags64 bit at index 23")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(23) {
		t.Error("failed .Get() unset Flags64 bit at index 23")
	}

	flags = 0

	flags |= mask
	if !flags.Get23() {
		t.Error("failed .Get23() set Flags64 bit at index 23")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get23() {
		t.Error("failed .Get23() unset Flags64 bit at index 23")
	}

	mask = bitutil.Flags64(1) << 24

	flags = 0

	flags |= mask
	if !flags.Get(24) {
		t.Error("failed .Get() set Flags64 bit at index 24")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(24) {
		t.Error("failed .Get() unset Flags64 bit at index 24")
	}

	flags = 0

	flags |= mask
	if !flags.Get24() {
		t.Error("failed .Get24() set Flags64 bit at index 24")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get24() {
		t.Error("failed .Get24() unset Flags64 bit at index 24")
	}

	mask = bitutil.Flags64(1) << 25

	flags = 0

	flags |= mask
	if !flags.Get(25) {
		t.Error("failed .Get() set Flags64 bit at index 25")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(25) {
		t.Error("failed .Get() unset Flags64 bit at index 25")
	}

	flags = 0

	flags |= mask
	if !flags.Get25() {
		t.Error("failed .Get25() set Flags64 bit at index 25")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get25() {
		t.Error("failed .Get25() unset Flags64 bit at index 25")
	}

	mask = bitutil.Flags64(1) << 26

	flags = 0

	flags |= mask
	if !flags.Get(26) {
		t.Error("failed .Get() set Flags64 bit at index 26")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(26) {
		t.Error("failed .Get() unset Flags64 bit at index 26")
	}

	flags = 0

	flags |= mask
	if !flags.Get26() {
		t.Error("failed .Get26() set Flags64 bit at index 26")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get26() {
		t.Error("failed .Get26() unset Flags64 bit at index 26")
	}

	mask = bitutil.Flags64(1) << 27

	flags = 0

	flags |= mask
	if !flags.Get(27) {
		t.Error("failed .Get() set Flags64 bit at index 27")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(27) {
		t.Error("failed .Get() unset Flags64 bit at index 27")
	}

	flags = 0

	flags |= mask
	if !flags.Get27() {
		t.Error("failed .Get27() set Flags64 bit at index 27")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get27() {
		t.Error("failed .Get27() unset Flags64 bit at index 27")
	}

	mask = bitutil.Flags64(1) << 28

	flags = 0

	flags |= mask
	if !flags.Get(28) {
		t.Error("failed .Get() set Flags64 bit at index 28")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(28) {
		t.Error("failed .Get() unset Flags64 bit at index 28")
	}

	flags = 0

	flags |= mask
	if !flags.Get28() {
		t.Error("failed .Get28() set Flags64 bit at index 28")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get28() {
		t.Error("failed .Get28() unset Flags64 bit at index 28")
	}

	mask = bitutil.Flags64(1) << 29

	flags = 0

	flags |= mask
	if !flags.Get(29) {
		t.Error("failed .Get() set Flags64 bit at index 29")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(29) {
		t.Error("failed .Get() unset Flags64 bit at index 29")
	}

	flags = 0

	flags |= mask
	if !flags.Get29() {
		t.Error("failed .Get29() set Flags64 bit at index 29")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get29() {
		t.Error("failed .Get29() unset Flags64 bit at index 29")
	}

	mask = bitutil.Flags64(1) << 30

	flags = 0

	flags |= mask
	if !flags.Get(30) {
		t.Error("failed .Get() set Flags64 bit at index 30")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(30) {
		t.Error("failed .Get() unset Flags64 bit at index 30")
	}

	flags = 0

	flags |= mask
	if !flags.Get30() {
		t.Error("failed .Get30() set Flags64 bit at index 30")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get30() {
		t.Error("failed .Get30() unset Flags64 bit at index 30")
	}

	mask = bitutil.Flags64(1) << 31

	flags = 0

	flags |= mask
	if !flags.Get(31) {
		t.Error("failed .Get() set Flags64 bit at index 31")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(31) {
		t.Error("failed .Get() unset Flags64 bit at index 31")
	}

	flags = 0

	flags |= mask
	if !flags.Get31() {
		t.Error("failed .Get31() set Flags64 bit at index 31")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get31() {
		t.Error("failed .Get31() unset Flags64 bit at index 31")
	}

	mask = bitutil.Flags64(1) << 32

	flags = 0

	flags |= mask
	if !flags.Get(32) {
		t.Error("failed .Get() set Flags64 bit at index 32")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(32) {
		t.Error("failed .Get() unset Flags64 bit at index 32")
	}

	flags = 0

	flags |= mask
	if !flags.Get32() {
		t.Error("failed .Get32() set Flags64 bit at index 32")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get32() {
		t.Error("failed .Get32() unset Flags64 bit at index 32")
	}

	mask = bitutil.Flags64(1) << 33

	flags = 0

	flags |= mask
	if !flags.Get(33) {
		t.Error("failed .Get() set Flags64 bit at index 33")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(33) {
		t.Error("failed .Get() unset Flags64 bit at index 33")
	}

	flags = 0

	flags |= mask
	if !flags.Get33() {
		t.Error("failed .Get33() set Flags64 bit at index 33")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get33() {
		t.Error("failed .Get33() unset Flags64 bit at index 33")
	}

	mask = bitutil.Flags64(1) << 34

	flags = 0

	flags |= mask
	if !flags.Get(34) {
		t.Error("failed .Get() set Flags64 bit at index 34")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(34) {
		t.Error("failed .Get() unset Flags64 bit at index 34")
	}

	flags = 0

	flags |= mask
	if !flags.Get34() {
		t.Error("failed .Get34() set Flags64 bit at index 34")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get34() {
		t.Error("failed .Get34() unset Flags64 bit at index 34")
	}

	mask = bitutil.Flags64(1) << 35

	flags = 0

	flags |= mask
	if !flags.Get(35) {
		t.Error("failed .Get() set Flags64 bit at index 35")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(35) {
		t.Error("failed .Get() unset Flags64 bit at index 35")
	}

	flags = 0

	flags |= mask
	if !flags.Get35() {
		t.Error("failed .Get35() set Flags64 bit at index 35")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get35() {
		t.Error("failed .Get35() unset Flags64 bit at index 35")
	}

	mask = bitutil.Flags64(1) << 36

	flags = 0

	flags |= mask
	if !flags.Get(36) {
		t.Error("failed .Get() set Flags64 bit at index 36")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(36) {
		t.Error("failed .Get() unset Flags64 bit at index 36")
	}

	flags = 0

	flags |= mask
	if !flags.Get36() {
		t.Error("failed .Get36() set Flags64 bit at index 36")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get36() {
		t.Error("failed .Get36() unset Flags64 bit at index 36")
	}

	mask = bitutil.Flags64(1) << 37

	flags = 0

	flags |= mask
	if !flags.Get(37) {
		t.Error("failed .Get() set Flags64 bit at index 37")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(37) {
		t.Error("failed .Get() unset Flags64 bit at index 37")
	}

	flags = 0

	flags |= mask
	if !flags.Get37() {
		t.Error("failed .Get37() set Flags64 bit at index 37")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get37() {
		t.Error("failed .Get37() unset Flags64 bit at index 37")
	}

	mask = bitutil.Flags64(1) << 38

	flags = 0

	flags |= mask
	if !flags.Get(38) {
		t.Error("failed .Get() set Flags64 bit at index 38")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(38) {
		t.Error("failed .Get() unset Flags64 bit at index 38")
	}

	flags = 0

	flags |= mask
	if !flags.Get38() {
		t.Error("failed .Get38() set Flags64 bit at index 38")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get38() {
		t.Error("failed .Get38() unset Flags64 bit at index 38")
	}

	mask = bitutil.Flags64(1) << 39

	flags = 0

	flags |= mask
	if !flags.Get(39) {
		t.Error("failed .Get() set Flags64 bit at index 39")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(39) {
		t.Error("failed .Get() unset Flags64 bit at index 39")
	}

	flags = 0

	flags |= mask
	if !flags.Get39() {
		t.Error("failed .Get39() set Flags64 bit at index 39")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get39() {
		t.Error("failed .Get39() unset Flags64 bit at index 39")
	}

	mask = bitutil.Flags64(1) << 40

	flags = 0

	flags |= mask
	if !flags.Get(40) {
		t.Error("failed .Get() set Flags64 bit at index 40")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(40) {
		t.Error("failed .Get() unset Flags64 bit at index 40")
	}

	flags = 0

	flags |= mask
	if !flags.Get40() {
		t.Error("failed .Get40() set Flags64 bit at index 40")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get40() {
		t.Error("failed .Get40() unset Flags64 bit at index 40")
	}

	mask = bitutil.Flags64(1) << 41

	flags = 0

	flags |= mask
	if !flags.Get(41) {
		t.Error("failed .Get() set Flags64 bit at index 41")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(41) {
		t.Error("failed .Get() unset Flags64 bit at index 41")
	}

	flags = 0

	flags |= mask
	if !flags.Get41() {
		t.Error("failed .Get41() set Flags64 bit at index 41")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get41() {
		t.Error("failed .Get41() unset Flags64 bit at index 41")
	}

	mask = bitutil.Flags64(1) << 42

	flags = 0

	flags |= mask
	if !flags.Get(42) {
		t.Error("failed .Get() set Flags64 bit at index 42")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(42) {
		t.Error("failed .Get() unset Flags64 bit at index 42")
	}

	flags = 0

	flags |= mask
	if !flags.Get42() {
		t.Error("failed .Get42() set Flags64 bit at index 42")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get42() {
		t.Error("failed .Get42() unset Flags64 bit at index 42")
	}

	mask = bitutil.Flags64(1) << 43

	flags = 0

	flags |= mask
	if !flags.Get(43) {
		t.Error("failed .Get() set Flags64 bit at index 43")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(43) {
		t.Error("failed .Get() unset Flags64 bit at index 43")
	}

	flags = 0

	flags |= mask
	if !flags.Get43() {
		t.Error("failed .Get43() set Flags64 bit at index 43")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get43() {
		t.Error("failed .Get43() unset Flags64 bit at index 43")
	}

	mask = bitutil.Flags64(1) << 44

	flags = 0

	flags |= mask
	if !flags.Get(44) {
		t.Error("failed .Get() set Flags64 bit at index 44")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(44) {
		t.Error("failed .Get() unset Flags64 bit at index 44")
	}

	flags = 0

	flags |= mask
	if !flags.Get44() {
		t.Error("failed .Get44() set Flags64 bit at index 44")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get44() {
		t.Error("failed .Get44() unset Flags64 bit at index 44")
	}

	mask = bitutil.Flags64(1) << 45

	flags = 0

	flags |= mask
	if !flags.Get(45) {
		t.Error("failed .Get() set Flags64 bit at index 45")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(45) {
		t.Error("failed .Get() unset Flags64 bit at index 45")
	}

	flags = 0

	flags |= mask
	if !flags.Get45() {
		t.Error("failed .Get45() set Flags64 bit at index 45")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get45() {
		t.Error("failed .Get45() unset Flags64 bit at index 45")
	}

	mask = bitutil.Flags64(1) << 46

	flags = 0

	flags |= mask
	if !flags.Get(46) {
		t.Error("failed .Get() set Flags64 bit at index 46")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(46) {
		t.Error("failed .Get() unset Flags64 bit at index 46")
	}

	flags = 0

	flags |= mask
	if !flags.Get46() {
		t.Error("failed .Get46() set Flags64 bit at index 46")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get46() {
		t.Error("failed .Get46() unset Flags64 bit at index 46")
	}

	mask = bitutil.Flags64(1) << 47

	flags = 0

	flags |= mask
	if !flags.Get(47) {
		t.Error("failed .Get() set Flags64 bit at index 47")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(47) {
		t.Error("failed .Get() unset Flags64 bit at index 47")
	}

	flags = 0

	flags |= mask
	if !flags.Get47() {
		t.Error("failed .Get47() set Flags64 bit at index 47")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get47() {
		t.Error("failed .Get47() unset Flags64 bit at index 47")
	}

	mask = bitutil.Flags64(1) << 48

	flags = 0

	flags |= mask
	if !flags.Get(48) {
		t.Error("failed .Get() set Flags64 bit at index 48")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(48) {
		t.Error("failed .Get() unset Flags64 bit at index 48")
	}

	flags = 0

	flags |= mask
	if !flags.Get48() {
		t.Error("failed .Get48() set Flags64 bit at index 48")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get48() {
		t.Error("failed .Get48() unset Flags64 bit at index 48")
	}

	mask = bitutil.Flags64(1) << 49

	flags = 0

	flags |= mask
	if !flags.Get(49) {
		t.Error("failed .Get() set Flags64 bit at index 49")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(49) {
		t.Error("failed .Get() unset Flags64 bit at index 49")
	}

	flags = 0

	flags |= mask
	if !flags.Get49() {
		t.Error("failed .Get49() set Flags64 bit at index 49")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get49() {
		t.Error("failed .Get49() unset Flags64 bit at index 49")
	}

	mask = bitutil.Flags64(1) << 50

	flags = 0

	flags |= mask
	if !flags.Get(50) {
		t.Error("failed .Get() set Flags64 bit at index 50")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(50) {
		t.Error("failed .Get() unset Flags64 bit at index 50")
	}

	flags = 0

	flags |= mask
	if !flags.Get50() {
		t.Error("failed .Get50() set Flags64 bit at index 50")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get50() {
		t.Error("failed .Get50() unset Flags64 bit at index 50")
	}

	mask = bitutil.Flags64(1) << 51

	flags = 0

	flags |= mask
	if !flags.Get(51) {
		t.Error("failed .Get() set Flags64 bit at index 51")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(51) {
		t.Error("failed .Get() unset Flags64 bit at index 51")
	}

	flags = 0

	flags |= mask
	if !flags.Get51() {
		t.Error("failed .Get51() set Flags64 bit at index 51")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get51() {
		t.Error("failed .Get51() unset Flags64 bit at index 51")
	}

	mask = bitutil.Flags64(1) << 52

	flags = 0

	flags |= mask
	if !flags.Get(52) {
		t.Error("failed .Get() set Flags64 bit at index 52")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(52) {
		t.Error("failed .Get() unset Flags64 bit at index 52")
	}

	flags = 0

	flags |= mask
	if !flags.Get52() {
		t.Error("failed .Get52() set Flags64 bit at index 52")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get52() {
		t.Error("failed .Get52() unset Flags64 bit at index 52")
	}

	mask = bitutil.Flags64(1) << 53

	flags = 0

	flags |= mask
	if !flags.Get(53) {
		t.Error("failed .Get() set Flags64 bit at index 53")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(53) {
		t.Error("failed .Get() unset Flags64 bit at index 53")
	}

	flags = 0

	flags |= mask
	if !flags.Get53() {
		t.Error("failed .Get53() set Flags64 bit at index 53")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get53() {
		t.Error("failed .Get53() unset Flags64 bit at index 53")
	}

	mask = bitutil.Flags64(1) << 54

	flags = 0

	flags |= mask
	if !flags.Get(54) {
		t.Error("failed .Get() set Flags64 bit at index 54")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(54) {
		t.Error("failed .Get() unset Flags64 bit at index 54")
	}

	flags = 0

	flags |= mask
	if !flags.Get54() {
		t.Error("failed .Get54() set Flags64 bit at index 54")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get54() {
		t.Error("failed .Get54() unset Flags64 bit at index 54")
	}

	mask = bitutil.Flags64(1) << 55

	flags = 0

	flags |= mask
	if !flags.Get(55) {
		t.Error("failed .Get() set Flags64 bit at index 55")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(55) {
		t.Error("failed .Get() unset Flags64 bit at index 55")
	}

	flags = 0

	flags |= mask
	if !flags.Get55() {
		t.Error("failed .Get55() set Flags64 bit at index 55")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get55() {
		t.Error("failed .Get55() unset Flags64 bit at index 55")
	}

	mask = bitutil.Flags64(1) << 56

	flags = 0

	flags |= mask
	if !flags.Get(56) {
		t.Error("failed .Get() set Flags64 bit at index 56")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(56) {
		t.Error("failed .Get() unset Flags64 bit at index 56")
	}

	flags = 0

	flags |= mask
	if !flags.Get56() {
		t.Error("failed .Get56() set Flags64 bit at index 56")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get56() {
		t.Error("failed .Get56() unset Flags64 bit at index 56")
	}

	mask = bitutil.Flags64(1) << 57

	flags = 0

	flags |= mask
	if !flags.Get(57) {
		t.Error("failed .Get() set Flags64 bit at index 57")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(57) {
		t.Error("failed .Get() unset Flags64 bit at index 57")
	}

	flags = 0

	flags |= mask
	if !flags.Get57() {
		t.Error("failed .Get57() set Flags64 bit at index 57")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get57() {
		t.Error("failed .Get57() unset Flags64 bit at index 57")
	}

	mask = bitutil.Flags64(1) << 58

	flags = 0

	flags |= mask
	if !flags.Get(58) {
		t.Error("failed .Get() set Flags64 bit at index 58")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(58) {
		t.Error("failed .Get() unset Flags64 bit at index 58")
	}

	flags = 0

	flags |= mask
	if !flags.Get58() {
		t.Error("failed .Get58() set Flags64 bit at index 58")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get58() {
		t.Error("failed .Get58() unset Flags64 bit at index 58")
	}

	mask = bitutil.Flags64(1) << 59

	flags = 0

	flags |= mask
	if !flags.Get(59) {
		t.Error("failed .Get() set Flags64 bit at index 59")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(59) {
		t.Error("failed .Get() unset Flags64 bit at index 59")
	}

	flags = 0

	flags |= mask
	if !flags.Get59() {
		t.Error("failed .Get59() set Flags64 bit at index 59")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get59() {
		t.Error("failed .Get59() unset Flags64 bit at index 59")
	}

	mask = bitutil.Flags64(1) << 60

	flags = 0

	flags |= mask
	if !flags.Get(60) {
		t.Error("failed .Get() set Flags64 bit at index 60")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(60) {
		t.Error("failed .Get() unset Flags64 bit at index 60")
	}

	flags = 0

	flags |= mask
	if !flags.Get60() {
		t.Error("failed .Get60() set Flags64 bit at index 60")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get60() {
		t.Error("failed .Get60() unset Flags64 bit at index 60")
	}

	mask = bitutil.Flags64(1) << 61

	flags = 0

	flags |= mask
	if !flags.Get(61) {
		t.Error("failed .Get() set Flags64 bit at index 61")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(61) {
		t.Error("failed .Get() unset Flags64 bit at index 61")
	}

	flags = 0

	flags |= mask
	if !flags.Get61() {
		t.Error("failed .Get61() set Flags64 bit at index 61")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get61() {
		t.Error("failed .Get61() unset Flags64 bit at index 61")
	}

	mask = bitutil.Flags64(1) << 62

	flags = 0

	flags |= mask
	if !flags.Get(62) {
		t.Error("failed .Get() set Flags64 bit at index 62")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(62) {
		t.Error("failed .Get() unset Flags64 bit at index 62")
	}

	flags = 0

	flags |= mask
	if !flags.Get62() {
		t.Error("failed .Get62() set Flags64 bit at index 62")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get62() {
		t.Error("failed .Get62() unset Flags64 bit at index 62")
	}

	mask = bitutil.Flags64(1) << 63

	flags = 0

	flags |= mask
	if !flags.Get(63) {
		t.Error("failed .Get() set Flags64 bit at index 63")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get(63) {
		t.Error("failed .Get() unset Flags64 bit at index 63")
	}

	flags = 0

	flags |= mask
	if !flags.Get63() {
		t.Error("failed .Get63() set Flags64 bit at index 63")
	}

	flags = ^bitutil.Flags64(0)

	flags &= ^mask
	if flags.Get63() {
		t.Error("failed .Get63() unset Flags64 bit at index 63")
	}
}

func TestFlags64Set(t *testing.T) {
	var mask, flags bitutil.Flags64

	mask = bitutil.Flags64(1) << 0

	flags = 0

	flags = flags.Set(0)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 0")
	}

	flags = 0

	flags = flags.Set0()
	if flags&mask == 0 {
		t.Error("failed .Set0() Flags64 bit at index 0")
	}

	mask = bitutil.Flags64(1) << 1

	flags = 0

	flags = flags.Set(1)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 1")
	}

	flags = 0

	flags = flags.Set1()
	if flags&mask == 0 {
		t.Error("failed .Set1() Flags64 bit at index 1")
	}

	mask = bitutil.Flags64(1) << 2

	flags = 0

	flags = flags.Set(2)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 2")
	}

	flags = 0

	flags = flags.Set2()
	if flags&mask == 0 {
		t.Error("failed .Set2() Flags64 bit at index 2")
	}

	mask = bitutil.Flags64(1) << 3

	flags = 0

	flags = flags.Set(3)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 3")
	}

	flags = 0

	flags = flags.Set3()
	if flags&mask == 0 {
		t.Error("failed .Set3() Flags64 bit at index 3")
	}

	mask = bitutil.Flags64(1) << 4

	flags = 0

	flags = flags.Set(4)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 4")
	}

	flags = 0

	flags = flags.Set4()
	if flags&mask == 0 {
		t.Error("failed .Set4() Flags64 bit at index 4")
	}

	mask = bitutil.Flags64(1) << 5

	flags = 0

	flags = flags.Set(5)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 5")
	}

	flags = 0

	flags = flags.Set5()
	if flags&mask == 0 {
		t.Error("failed .Set5() Flags64 bit at index 5")
	}

	mask = bitutil.Flags64(1) << 6

	flags = 0

	flags = flags.Set(6)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 6")
	}

	flags = 0

	flags = flags.Set6()
	if flags&mask == 0 {
		t.Error("failed .Set6() Flags64 bit at index 6")
	}

	mask = bitutil.Flags64(1) << 7

	flags = 0

	flags = flags.Set(7)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 7")
	}

	flags = 0

	flags = flags.Set7()
	if flags&mask == 0 {
		t.Error("failed .Set7() Flags64 bit at index 7")
	}

	mask = bitutil.Flags64(1) << 8

	flags = 0

	flags = flags.Set(8)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 8")
	}

	flags = 0

	flags = flags.Set8()
	if flags&mask == 0 {
		t.Error("failed .Set8() Flags64 bit at index 8")
	}

	mask = bitutil.Flags64(1) << 9

	flags = 0

	flags = flags.Set(9)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 9")
	}

	flags = 0

	flags = flags.Set9()
	if flags&mask == 0 {
		t.Error("failed .Set9() Flags64 bit at index 9")
	}

	mask = bitutil.Flags64(1) << 10

	flags = 0

	flags = flags.Set(10)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 10")
	}

	flags = 0

	flags = flags.Set10()
	if flags&mask == 0 {
		t.Error("failed .Set10() Flags64 bit at index 10")
	}

	mask = bitutil.Flags64(1) << 11

	flags = 0

	flags = flags.Set(11)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 11")
	}

	flags = 0

	flags = flags.Set11()
	if flags&mask == 0 {
		t.Error("failed .Set11() Flags64 bit at index 11")
	}

	mask = bitutil.Flags64(1) << 12

	flags = 0

	flags = flags.Set(12)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 12")
	}

	flags = 0

	flags = flags.Set12()
	if flags&mask == 0 {
		t.Error("failed .Set12() Flags64 bit at index 12")
	}

	mask = bitutil.Flags64(1) << 13

	flags = 0

	flags = flags.Set(13)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 13")
	}

	flags = 0

	flags = flags.Set13()
	if flags&mask == 0 {
		t.Error("failed .Set13() Flags64 bit at index 13")
	}

	mask = bitutil.Flags64(1) << 14

	flags = 0

	flags = flags.Set(14)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 14")
	}

	flags = 0

	flags = flags.Set14()
	if flags&mask == 0 {
		t.Error("failed .Set14() Flags64 bit at index 14")
	}

	mask = bitutil.Flags64(1) << 15

	flags = 0

	flags = flags.Set(15)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 15")
	}

	flags = 0

	flags = flags.Set15()
	if flags&mask == 0 {
		t.Error("failed .Set15() Flags64 bit at index 15")
	}

	mask = bitutil.Flags64(1) << 16

	flags = 0

	flags = flags.Set(16)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 16")
	}

	flags = 0

	flags = flags.Set16()
	if flags&mask == 0 {
		t.Error("failed .Set16() Flags64 bit at index 16")
	}

	mask = bitutil.Flags64(1) << 17

	flags = 0

	flags = flags.Set(17)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 17")
	}

	flags = 0

	flags = flags.Set17()
	if flags&mask == 0 {
		t.Error("failed .Set17() Flags64 bit at index 17")
	}

	mask = bitutil.Flags64(1) << 18

	flags = 0

	flags = flags.Set(18)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 18")
	}

	flags = 0

	flags = flags.Set18()
	if flags&mask == 0 {
		t.Error("failed .Set18() Flags64 bit at index 18")
	}

	mask = bitutil.Flags64(1) << 19

	flags = 0

	flags = flags.Set(19)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 19")
	}

	flags = 0

	flags = flags.Set19()
	if flags&mask == 0 {
		t.Error("failed .Set19() Flags64 bit at index 19")
	}

	mask = bitutil.Flags64(1) << 20

	flags = 0

	flags = flags.Set(20)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 20")
	}

	flags = 0

	flags = flags.Set20()
	if flags&mask == 0 {
		t.Error("failed .Set20() Flags64 bit at index 20")
	}

	mask = bitutil.Flags64(1) << 21

	flags = 0

	flags = flags.Set(21)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 21")
	}

	flags = 0

	flags = flags.Set21()
	if flags&mask == 0 {
		t.Error("failed .Set21() Flags64 bit at index 21")
	}

	mask = bitutil.Flags64(1) << 22

	flags = 0

	flags = flags.Set(22)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 22")
	}

	flags = 0

	flags = flags.Set22()
	if flags&mask == 0 {
		t.Error("failed .Set22() Flags64 bit at index 22")
	}

	mask = bitutil.Flags64(1) << 23

	flags = 0

	flags = flags.Set(23)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 23")
	}

	flags = 0

	flags = flags.Set23()
	if flags&mask == 0 {
		t.Error("failed .Set23() Flags64 bit at index 23")
	}

	mask = bitutil.Flags64(1) << 24

	flags = 0

	flags = flags.Set(24)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 24")
	}

	flags = 0

	flags = flags.Set24()
	if flags&mask == 0 {
		t.Error("failed .Set24() Flags64 bit at index 24")
	}

	mask = bitutil.Flags64(1) << 25

	flags = 0

	flags = flags.Set(25)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 25")
	}

	flags = 0

	flags = flags.Set25()
	if flags&mask == 0 {
		t.Error("failed .Set25() Flags64 bit at index 25")
	}

	mask = bitutil.Flags64(1) << 26

	flags = 0

	flags = flags.Set(26)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 26")
	}

	flags = 0

	flags = flags.Set26()
	if flags&mask == 0 {
		t.Error("failed .Set26() Flags64 bit at index 26")
	}

	mask = bitutil.Flags64(1) << 27

	flags = 0

	flags = flags.Set(27)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 27")
	}

	flags = 0

	flags = flags.Set27()
	if flags&mask == 0 {
		t.Error("failed .Set27() Flags64 bit at index 27")
	}

	mask = bitutil.Flags64(1) << 28

	flags = 0

	flags = flags.Set(28)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 28")
	}

	flags = 0

	flags = flags.Set28()
	if flags&mask == 0 {
		t.Error("failed .Set28() Flags64 bit at index 28")
	}

	mask = bitutil.Flags64(1) << 29

	flags = 0

	flags = flags.Set(29)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 29")
	}

	flags = 0

	flags = flags.Set29()
	if flags&mask == 0 {
		t.Error("failed .Set29() Flags64 bit at index 29")
	}

	mask = bitutil.Flags64(1) << 30

	flags = 0

	flags = flags.Set(30)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 30")
	}

	flags = 0

	flags = flags.Set30()
	if flags&mask == 0 {
		t.Error("failed .Set30() Flags64 bit at index 30")
	}

	mask = bitutil.Flags64(1) << 31

	flags = 0

	flags = flags.Set(31)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 31")
	}

	flags = 0

	flags = flags.Set31()
	if flags&mask == 0 {
		t.Error("failed .Set31() Flags64 bit at index 31")
	}

	mask = bitutil.Flags64(1) << 32

	flags = 0

	flags = flags.Set(32)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 32")
	}

	flags = 0

	flags = flags.Set32()
	if flags&mask == 0 {
		t.Error("failed .Set32() Flags64 bit at index 32")
	}

	mask = bitutil.Flags64(1) << 33

	flags = 0

	flags = flags.Set(33)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 33")
	}

	flags = 0

	flags = flags.Set33()
	if flags&mask == 0 {
		t.Error("failed .Set33() Flags64 bit at index 33")
	}

	mask = bitutil.Flags64(1) << 34

	flags = 0

	flags = flags.Set(34)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 34")
	}

	flags = 0

	flags = flags.Set34()
	if flags&mask == 0 {
		t.Error("failed .Set34() Flags64 bit at index 34")
	}

	mask = bitutil.Flags64(1) << 35

	flags = 0

	flags = flags.Set(35)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 35")
	}

	flags = 0

	flags = flags.Set35()
	if flags&mask == 0 {
		t.Error("failed .Set35() Flags64 bit at index 35")
	}

	mask = bitutil.Flags64(1) << 36

	flags = 0

	flags = flags.Set(36)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 36")
	}

	flags = 0

	flags = flags.Set36()
	if flags&mask == 0 {
		t.Error("failed .Set36() Flags64 bit at index 36")
	}

	mask = bitutil.Flags64(1) << 37

	flags = 0

	flags = flags.Set(37)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 37")
	}

	flags = 0

	flags = flags.Set37()
	if flags&mask == 0 {
		t.Error("failed .Set37() Flags64 bit at index 37")
	}

	mask = bitutil.Flags64(1) << 38

	flags = 0

	flags = flags.Set(38)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 38")
	}

	flags = 0

	flags = flags.Set38()
	if flags&mask == 0 {
		t.Error("failed .Set38() Flags64 bit at index 38")
	}

	mask = bitutil.Flags64(1) << 39

	flags = 0

	flags = flags.Set(39)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 39")
	}

	flags = 0

	flags = flags.Set39()
	if flags&mask == 0 {
		t.Error("failed .Set39() Flags64 bit at index 39")
	}

	mask = bitutil.Flags64(1) << 40

	flags = 0

	flags = flags.Set(40)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 40")
	}

	flags = 0

	flags = flags.Set40()
	if flags&mask == 0 {
		t.Error("failed .Set40() Flags64 bit at index 40")
	}

	mask = bitutil.Flags64(1) << 41

	flags = 0

	flags = flags.Set(41)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 41")
	}

	flags = 0

	flags = flags.Set41()
	if flags&mask == 0 {
		t.Error("failed .Set41() Flags64 bit at index 41")
	}

	mask = bitutil.Flags64(1) << 42

	flags = 0

	flags = flags.Set(42)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 42")
	}

	flags = 0

	flags = flags.Set42()
	if flags&mask == 0 {
		t.Error("failed .Set42() Flags64 bit at index 42")
	}

	mask = bitutil.Flags64(1) << 43

	flags = 0

	flags = flags.Set(43)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 43")
	}

	flags = 0

	flags = flags.Set43()
	if flags&mask == 0 {
		t.Error("failed .Set43() Flags64 bit at index 43")
	}

	mask = bitutil.Flags64(1) << 44

	flags = 0

	flags = flags.Set(44)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 44")
	}

	flags = 0

	flags = flags.Set44()
	if flags&mask == 0 {
		t.Error("failed .Set44() Flags64 bit at index 44")
	}

	mask = bitutil.Flags64(1) << 45

	flags = 0

	flags = flags.Set(45)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 45")
	}

	flags = 0

	flags = flags.Set45()
	if flags&mask == 0 {
		t.Error("failed .Set45() Flags64 bit at index 45")
	}

	mask = bitutil.Flags64(1) << 46

	flags = 0

	flags = flags.Set(46)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 46")
	}

	flags = 0

	flags = flags.Set46()
	if flags&mask == 0 {
		t.Error("failed .Set46() Flags64 bit at index 46")
	}

	mask = bitutil.Flags64(1) << 47

	flags = 0

	flags = flags.Set(47)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 47")
	}

	flags = 0

	flags = flags.Set47()
	if flags&mask == 0 {
		t.Error("failed .Set47() Flags64 bit at index 47")
	}

	mask = bitutil.Flags64(1) << 48

	flags = 0

	flags = flags.Set(48)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 48")
	}

	flags = 0

	flags = flags.Set48()
	if flags&mask == 0 {
		t.Error("failed .Set48() Flags64 bit at index 48")
	}

	mask = bitutil.Flags64(1) << 49

	flags = 0

	flags = flags.Set(49)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 49")
	}

	flags = 0

	flags = flags.Set49()
	if flags&mask == 0 {
		t.Error("failed .Set49() Flags64 bit at index 49")
	}

	mask = bitutil.Flags64(1) << 50

	flags = 0

	flags = flags.Set(50)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 50")
	}

	flags = 0

	flags = flags.Set50()
	if flags&mask == 0 {
		t.Error("failed .Set50() Flags64 bit at index 50")
	}

	mask = bitutil.Flags64(1) << 51

	flags = 0

	flags = flags.Set(51)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 51")
	}

	flags = 0

	flags = flags.Set51()
	if flags&mask == 0 {
		t.Error("failed .Set51() Flags64 bit at index 51")
	}

	mask = bitutil.Flags64(1) << 52

	flags = 0

	flags = flags.Set(52)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 52")
	}

	flags = 0

	flags = flags.Set52()
	if flags&mask == 0 {
		t.Error("failed .Set52() Flags64 bit at index 52")
	}

	mask = bitutil.Flags64(1) << 53

	flags = 0

	flags = flags.Set(53)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 53")
	}

	flags = 0

	flags = flags.Set53()
	if flags&mask == 0 {
		t.Error("failed .Set53() Flags64 bit at index 53")
	}

	mask = bitutil.Flags64(1) << 54

	flags = 0

	flags = flags.Set(54)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 54")
	}

	flags = 0

	flags = flags.Set54()
	if flags&mask == 0 {
		t.Error("failed .Set54() Flags64 bit at index 54")
	}

	mask = bitutil.Flags64(1) << 55

	flags = 0

	flags = flags.Set(55)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 55")
	}

	flags = 0

	flags = flags.Set55()
	if flags&mask == 0 {
		t.Error("failed .Set55() Flags64 bit at index 55")
	}

	mask = bitutil.Flags64(1) << 56

	flags = 0

	flags = flags.Set(56)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 56")
	}

	flags = 0

	flags = flags.Set56()
	if flags&mask == 0 {
		t.Error("failed .Set56() Flags64 bit at index 56")
	}

	mask = bitutil.Flags64(1) << 57

	flags = 0

	flags = flags.Set(57)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 57")
	}

	flags = 0

	flags = flags.Set57()
	if flags&mask == 0 {
		t.Error("failed .Set57() Flags64 bit at index 57")
	}

	mask = bitutil.Flags64(1) << 58

	flags = 0

	flags = flags.Set(58)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 58")
	}

	flags = 0

	flags = flags.Set58()
	if flags&mask == 0 {
		t.Error("failed .Set58() Flags64 bit at index 58")
	}

	mask = bitutil.Flags64(1) << 59

	flags = 0

	flags = flags.Set(59)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 59")
	}

	flags = 0

	flags = flags.Set59()
	if flags&mask == 0 {
		t.Error("failed .Set59() Flags64 bit at index 59")
	}

	mask = bitutil.Flags64(1) << 60

	flags = 0

	flags = flags.Set(60)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 60")
	}

	flags = 0

	flags = flags.Set60()
	if flags&mask == 0 {
		t.Error("failed .Set60() Flags64 bit at index 60")
	}

	mask = bitutil.Flags64(1) << 61

	flags = 0

	flags = flags.Set(61)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 61")
	}

	flags = 0

	flags = flags.Set61()
	if flags&mask == 0 {
		t.Error("failed .Set61() Flags64 bit at index 61")
	}

	mask = bitutil.Flags64(1) << 62

	flags = 0

	flags = flags.Set(62)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 62")
	}

	flags = 0

	flags = flags.Set62()
	if flags&mask == 0 {
		t.Error("failed .Set62() Flags64 bit at index 62")
	}

	mask = bitutil.Flags64(1) << 63

	flags = 0

	flags = flags.Set(63)
	if flags&mask == 0 {
		t.Error("failed .Set() Flags64 bit at index 63")
	}

	flags = 0

	flags = flags.Set63()
	if flags&mask == 0 {
		t.Error("failed .Set63() Flags64 bit at index 63")
	}
}

func TestFlags64Unset(t *testing.T) {
	var mask, flags bitutil.Flags64

	mask = bitutil.Flags64(1) << 0

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(0)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 0")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset0()
	if flags&mask != 0 {
		t.Error("failed .Unset0() Flags64 bit at index 0")
	}

	mask = bitutil.Flags64(1) << 1

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(1)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 1")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset1()
	if flags&mask != 0 {
		t.Error("failed .Unset1() Flags64 bit at index 1")
	}

	mask = bitutil.Flags64(1) << 2

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(2)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 2")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset2()
	if flags&mask != 0 {
		t.Error("failed .Unset2() Flags64 bit at index 2")
	}

	mask = bitutil.Flags64(1) << 3

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(3)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 3")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset3()
	if flags&mask != 0 {
		t.Error("failed .Unset3() Flags64 bit at index 3")
	}

	mask = bitutil.Flags64(1) << 4

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(4)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 4")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset4()
	if flags&mask != 0 {
		t.Error("failed .Unset4() Flags64 bit at index 4")
	}

	mask = bitutil.Flags64(1) << 5

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(5)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 5")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset5()
	if flags&mask != 0 {
		t.Error("failed .Unset5() Flags64 bit at index 5")
	}

	mask = bitutil.Flags64(1) << 6

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(6)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 6")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset6()
	if flags&mask != 0 {
		t.Error("failed .Unset6() Flags64 bit at index 6")
	}

	mask = bitutil.Flags64(1) << 7

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(7)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 7")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset7()
	if flags&mask != 0 {
		t.Error("failed .Unset7() Flags64 bit at index 7")
	}

	mask = bitutil.Flags64(1) << 8

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(8)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 8")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset8()
	if flags&mask != 0 {
		t.Error("failed .Unset8() Flags64 bit at index 8")
	}

	mask = bitutil.Flags64(1) << 9

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(9)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 9")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset9()
	if flags&mask != 0 {
		t.Error("failed .Unset9() Flags64 bit at index 9")
	}

	mask = bitutil.Flags64(1) << 10

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(10)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 10")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset10()
	if flags&mask != 0 {
		t.Error("failed .Unset10() Flags64 bit at index 10")
	}

	mask = bitutil.Flags64(1) << 11

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(11)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 11")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset11()
	if flags&mask != 0 {
		t.Error("failed .Unset11() Flags64 bit at index 11")
	}

	mask = bitutil.Flags64(1) << 12

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(12)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 12")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset12()
	if flags&mask != 0 {
		t.Error("failed .Unset12() Flags64 bit at index 12")
	}

	mask = bitutil.Flags64(1) << 13

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(13)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 13")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset13()
	if flags&mask != 0 {
		t.Error("failed .Unset13() Flags64 bit at index 13")
	}

	mask = bitutil.Flags64(1) << 14

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(14)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 14")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset14()
	if flags&mask != 0 {
		t.Error("failed .Unset14() Flags64 bit at index 14")
	}

	mask = bitutil.Flags64(1) << 15

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(15)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 15")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset15()
	if flags&mask != 0 {
		t.Error("failed .Unset15() Flags64 bit at index 15")
	}

	mask = bitutil.Flags64(1) << 16

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(16)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 16")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset16()
	if flags&mask != 0 {
		t.Error("failed .Unset16() Flags64 bit at index 16")
	}

	mask = bitutil.Flags64(1) << 17

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(17)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 17")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset17()
	if flags&mask != 0 {
		t.Error("failed .Unset17() Flags64 bit at index 17")
	}

	mask = bitutil.Flags64(1) << 18

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(18)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 18")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset18()
	if flags&mask != 0 {
		t.Error("failed .Unset18() Flags64 bit at index 18")
	}

	mask = bitutil.Flags64(1) << 19

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(19)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 19")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset19()
	if flags&mask != 0 {
		t.Error("failed .Unset19() Flags64 bit at index 19")
	}

	mask = bitutil.Flags64(1) << 20

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(20)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 20")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset20()
	if flags&mask != 0 {
		t.Error("failed .Unset20() Flags64 bit at index 20")
	}

	mask = bitutil.Flags64(1) << 21

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(21)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 21")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset21()
	if flags&mask != 0 {
		t.Error("failed .Unset21() Flags64 bit at index 21")
	}

	mask = bitutil.Flags64(1) << 22

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(22)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 22")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset22()
	if flags&mask != 0 {
		t.Error("failed .Unset22() Flags64 bit at index 22")
	}

	mask = bitutil.Flags64(1) << 23

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(23)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 23")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset23()
	if flags&mask != 0 {
		t.Error("failed .Unset23() Flags64 bit at index 23")
	}

	mask = bitutil.Flags64(1) << 24

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(24)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 24")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset24()
	if flags&mask != 0 {
		t.Error("failed .Unset24() Flags64 bit at index 24")
	}

	mask = bitutil.Flags64(1) << 25

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(25)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 25")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset25()
	if flags&mask != 0 {
		t.Error("failed .Unset25() Flags64 bit at index 25")
	}

	mask = bitutil.Flags64(1) << 26

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(26)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 26")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset26()
	if flags&mask != 0 {
		t.Error("failed .Unset26() Flags64 bit at index 26")
	}

	mask = bitutil.Flags64(1) << 27

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(27)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 27")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset27()
	if flags&mask != 0 {
		t.Error("failed .Unset27() Flags64 bit at index 27")
	}

	mask = bitutil.Flags64(1) << 28

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(28)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 28")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset28()
	if flags&mask != 0 {
		t.Error("failed .Unset28() Flags64 bit at index 28")
	}

	mask = bitutil.Flags64(1) << 29

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(29)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 29")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset29()
	if flags&mask != 0 {
		t.Error("failed .Unset29() Flags64 bit at index 29")
	}

	mask = bitutil.Flags64(1) << 30

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(30)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 30")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset30()
	if flags&mask != 0 {
		t.Error("failed .Unset30() Flags64 bit at index 30")
	}

	mask = bitutil.Flags64(1) << 31

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(31)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 31")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset31()
	if flags&mask != 0 {
		t.Error("failed .Unset31() Flags64 bit at index 31")
	}

	mask = bitutil.Flags64(1) << 32

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(32)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 32")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset32()
	if flags&mask != 0 {
		t.Error("failed .Unset32() Flags64 bit at index 32")
	}

	mask = bitutil.Flags64(1) << 33

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(33)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 33")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset33()
	if flags&mask != 0 {
		t.Error("failed .Unset33() Flags64 bit at index 33")
	}

	mask = bitutil.Flags64(1) << 34

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(34)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 34")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset34()
	if flags&mask != 0 {
		t.Error("failed .Unset34() Flags64 bit at index 34")
	}

	mask = bitutil.Flags64(1) << 35

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(35)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 35")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset35()
	if flags&mask != 0 {
		t.Error("failed .Unset35() Flags64 bit at index 35")
	}

	mask = bitutil.Flags64(1) << 36

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(36)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 36")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset36()
	if flags&mask != 0 {
		t.Error("failed .Unset36() Flags64 bit at index 36")
	}

	mask = bitutil.Flags64(1) << 37

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(37)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 37")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset37()
	if flags&mask != 0 {
		t.Error("failed .Unset37() Flags64 bit at index 37")
	}

	mask = bitutil.Flags64(1) << 38

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(38)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 38")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset38()
	if flags&mask != 0 {
		t.Error("failed .Unset38() Flags64 bit at index 38")
	}

	mask = bitutil.Flags64(1) << 39

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(39)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 39")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset39()
	if flags&mask != 0 {
		t.Error("failed .Unset39() Flags64 bit at index 39")
	}

	mask = bitutil.Flags64(1) << 40

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(40)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 40")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset40()
	if flags&mask != 0 {
		t.Error("failed .Unset40() Flags64 bit at index 40")
	}

	mask = bitutil.Flags64(1) << 41

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(41)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 41")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset41()
	if flags&mask != 0 {
		t.Error("failed .Unset41() Flags64 bit at index 41")
	}

	mask = bitutil.Flags64(1) << 42

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(42)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 42")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset42()
	if flags&mask != 0 {
		t.Error("failed .Unset42() Flags64 bit at index 42")
	}

	mask = bitutil.Flags64(1) << 43

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(43)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 43")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset43()
	if flags&mask != 0 {
		t.Error("failed .Unset43() Flags64 bit at index 43")
	}

	mask = bitutil.Flags64(1) << 44

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(44)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 44")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset44()
	if flags&mask != 0 {
		t.Error("failed .Unset44() Flags64 bit at index 44")
	}

	mask = bitutil.Flags64(1) << 45

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(45)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 45")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset45()
	if flags&mask != 0 {
		t.Error("failed .Unset45() Flags64 bit at index 45")
	}

	mask = bitutil.Flags64(1) << 46

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(46)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 46")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset46()
	if flags&mask != 0 {
		t.Error("failed .Unset46() Flags64 bit at index 46")
	}

	mask = bitutil.Flags64(1) << 47

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(47)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 47")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset47()
	if flags&mask != 0 {
		t.Error("failed .Unset47() Flags64 bit at index 47")
	}

	mask = bitutil.Flags64(1) << 48

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(48)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 48")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset48()
	if flags&mask != 0 {
		t.Error("failed .Unset48() Flags64 bit at index 48")
	}

	mask = bitutil.Flags64(1) << 49

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(49)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 49")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset49()
	if flags&mask != 0 {
		t.Error("failed .Unset49() Flags64 bit at index 49")
	}

	mask = bitutil.Flags64(1) << 50

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(50)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 50")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset50()
	if flags&mask != 0 {
		t.Error("failed .Unset50() Flags64 bit at index 50")
	}

	mask = bitutil.Flags64(1) << 51

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(51)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 51")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset51()
	if flags&mask != 0 {
		t.Error("failed .Unset51() Flags64 bit at index 51")
	}

	mask = bitutil.Flags64(1) << 52

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(52)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 52")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset52()
	if flags&mask != 0 {
		t.Error("failed .Unset52() Flags64 bit at index 52")
	}

	mask = bitutil.Flags64(1) << 53

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(53)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 53")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset53()
	if flags&mask != 0 {
		t.Error("failed .Unset53() Flags64 bit at index 53")
	}

	mask = bitutil.Flags64(1) << 54

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(54)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 54")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset54()
	if flags&mask != 0 {
		t.Error("failed .Unset54() Flags64 bit at index 54")
	}

	mask = bitutil.Flags64(1) << 55

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(55)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 55")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset55()
	if flags&mask != 0 {
		t.Error("failed .Unset55() Flags64 bit at index 55")
	}

	mask = bitutil.Flags64(1) << 56

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(56)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 56")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset56()
	if flags&mask != 0 {
		t.Error("failed .Unset56() Flags64 bit at index 56")
	}

	mask = bitutil.Flags64(1) << 57

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(57)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 57")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset57()
	if flags&mask != 0 {
		t.Error("failed .Unset57() Flags64 bit at index 57")
	}

	mask = bitutil.Flags64(1) << 58

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(58)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 58")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset58()
	if flags&mask != 0 {
		t.Error("failed .Unset58() Flags64 bit at index 58")
	}

	mask = bitutil.Flags64(1) << 59

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(59)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 59")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset59()
	if flags&mask != 0 {
		t.Error("failed .Unset59() Flags64 bit at index 59")
	}

	mask = bitutil.Flags64(1) << 60

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(60)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 60")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset60()
	if flags&mask != 0 {
		t.Error("failed .Unset60() Flags64 bit at index 60")
	}

	mask = bitutil.Flags64(1) << 61

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(61)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 61")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset61()
	if flags&mask != 0 {
		t.Error("failed .Unset61() Flags64 bit at index 61")
	}

	mask = bitutil.Flags64(1) << 62

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(62)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 62")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset62()
	if flags&mask != 0 {
		t.Error("failed .Unset62() Flags64 bit at index 62")
	}

	mask = bitutil.Flags64(1) << 63

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset(63)
	if flags&mask != 0 {
		t.Error("failed .Unset() Flags64 bit at index 63")
	}

	flags = ^bitutil.Flags64(0)

	flags = flags.Unset63()
	if flags&mask != 0 {
		t.Error("failed .Unset63() Flags64 bit at index 63")
	}
}
