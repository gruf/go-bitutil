package main

import (
	"bytes"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"text/template"
)

func must(t *template.Template, e error) *template.Template {
	if e != nil {
		panic(e)
	}
	return t
}

func main() {
	// Define base tpl functions.
	funcs := template.FuncMap{
		"sub": func(a, b int) int {
			return a - b
		},
	}

	// Load main bit flags template file
	flagTpl := must(template.New("flag.tpl").Funcs(funcs).ParseFiles("flag.tpl"))

	// Load bit flags tests template file
	tflagTpl := must(template.New("flag_test.tpl").Funcs(funcs).ParseFiles("flag_test.tpl"))

	flags := []struct {
		Size int
		Bits []int
	}{
		{Size: 8, Bits: bits(8)},
		{Size: 16, Bits: bits(16)},
		{Size: 32, Bits: bits(32)},
		{Size: 64, Bits: bits(64)},
	}

	// Generate bit flags Go file
	if err := generate(flagTpl, "flag.go", flags); err != nil {
		panic(err)
	}

	// Generate bit flags Go tests file
	if err := generate(tflagTpl, "flag_test.go", flags); err != nil {
		panic(err)
	}
}

func generate(tpl *template.Template, filename string, data interface{}) error {
	var buf bytes.Buffer

	// Execute the template for atomics type
	if err := tpl.Execute(&buf, data); err != nil {
		return fmt.Errorf("error executing template for '%s': %v", filename, err)
	}

	// Open file for the atomic type
	file, err := os.OpenFile(filename, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0o644)
	if err != nil {
		return fmt.Errorf("error opening file: %v", err)
	}

	// Write generated buffer contents to file
	if _, err := buf.WriteTo(file); err != nil {
		return errors.New("error writing to file '" + filename + "'")
	} else if err := file.Close(); err != nil {
		return errors.New("error closing file '" + filename + "'")
	}

	// Run gofmt on generated file
	cmd := exec.Command("gofumports", "-w", filename)
	if err := cmd.Run(); err != nil {
		return errors.New("error running gofmt -w " + filename)
	}

	return nil
}

func bits(count int) []int {
	var bits []int
	for i := 0; i < count; i++ {
		bits = append(bits, i)
	}
	return bits
}
