package bitutil_test

import (
	"math"
	"testing"

	"codeberg.org/gruf/go-bitutil"
)

func TestAbs8(t *testing.T) {
	for i := int8(-127); i < 127; i++ {
		if int8(math.Abs(float64(i))) != bitutil.Abs8(i) {
			t.Error("did not calculate expected absolute value")
		}
	}
}

func TestAbs16(t *testing.T) {
	for i := int16(-127); i < 127; i++ {
		if int16(math.Abs(float64(i))) != bitutil.Abs16(i) {
			t.Error("did not calculate expected absolute value")
		}
	}
}

func TestAbs32(t *testing.T) {
	for i := int32(-127); i < 127; i++ {
		if int32(math.Abs(float64(i))) != bitutil.Abs32(i) {
			t.Error("did not calculate expected absolute value")
		}
	}
}

func TestAbs64(t *testing.T) {
	for i := int64(-127); i < 127; i++ {
		if int64(math.Abs(float64(i))) != bitutil.Abs64(i) {
			t.Error("did not calculate expected absolute value")
		}
	}
}
