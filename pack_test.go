package bitutil_test

import (
	"testing"

	"codeberg.org/gruf/go-bitutil"
)

func TestPackInt8s(t *testing.T) {
	i1, i2 := int8(-1<<7), int8(-1<<7)
	t1, t2 := bitutil.UnpackInt8s(bitutil.PackInt8s(i1, i2))
	if i1 != t1 || i2 != t2 {
		t.Error("failed packing/unpacking int8s")
	}

	i1, i2 = int8(-1<<7), int8(1<<7-1)
	t1, t2 = bitutil.UnpackInt8s(bitutil.PackInt8s(i1, i2))
	if i1 != t1 || i2 != t2 {
		t.Error("failed packing/unpacking int8s")
	}

	i1, i2 = int8(1<<7-1), int8(1<<7-1)
	t1, t2 = bitutil.UnpackInt8s(bitutil.PackInt8s(i1, i2))
	if i1 != t1 || i2 != t2 {
		t.Error("failed packing/unpacking int8s")
	}

	i1, i2 = int8(1<<7-1), int8(-1<<7)
	t1, t2 = bitutil.UnpackInt8s(bitutil.PackInt8s(i1, i2))
	if i1 != t1 || i2 != t2 {
		t.Error("failed packing/unpacking int8s")
	}
}

func TestPackInt16s(t *testing.T) {
	i1, i2 := int16(-1<<15), int16(-1<<15)
	t1, t2 := bitutil.UnpackInt16s(bitutil.PackInt16s(i1, i2))
	if i1 != t1 || i2 != t2 {
		t.Error("failed packing/unpacking int16s")
	}

	i1, i2 = int16(-1<<15), int16(1<<15-1)
	t1, t2 = bitutil.UnpackInt16s(bitutil.PackInt16s(i1, i2))
	if i1 != t1 || i2 != t2 {
		t.Error("failed packing/unpacking int16s")
	}

	i1, i2 = int16(1<<15-1), int16(1<<15-1)
	t1, t2 = bitutil.UnpackInt16s(bitutil.PackInt16s(i1, i2))
	if i1 != t1 || i2 != t2 {
		t.Error("failed packing/unpacking int16s")
	}

	i1, i2 = int16(1<<15-1), int16(-1<<15)
	t1, t2 = bitutil.UnpackInt16s(bitutil.PackInt16s(i1, i2))
	if i1 != t1 || i2 != t2 {
		t.Error("failed packing/unpacking int16s")
	}
}

func TestPackInt32s(t *testing.T) {
	i1, i2 := int32(-1<<31), int32(-1<<31)
	t1, t2 := bitutil.UnpackInt32s(bitutil.PackInt32s(i1, i2))
	if i1 != t1 || i2 != t2 {
		t.Error("failed packing/unpacking int32s")
	}

	i1, i2 = int32(-1<<31), int32(1<<31-1)
	t1, t2 = bitutil.UnpackInt32s(bitutil.PackInt32s(i1, i2))
	if i1 != t1 || i2 != t2 {
		t.Error("failed packing/unpacking int32s")
	}

	i1, i2 = int32(1<<31-1), int32(1<<31-1)
	t1, t2 = bitutil.UnpackInt32s(bitutil.PackInt32s(i1, i2))
	if i1 != t1 || i2 != t2 {
		t.Error("failed packing/unpacking int32s")
	}

	i1, i2 = int32(1<<31-1), int32(-1<<31)
	t1, t2 = bitutil.UnpackInt32s(bitutil.PackInt32s(i1, i2))
	if i1 != t1 || i2 != t2 {
		t.Error("failed packing/unpacking int32s")
	}
}

func TestPackUint8s(t *testing.T) {
	u1, u2 := uint8(0), uint8(0)
	t1, t2 := bitutil.UnpackUint8s(bitutil.PackUint8s(u1, u2))
	if u1 != t1 || u2 != t2 {
		t.Error("failed packing/unpacking uint8s")
	}

	u1, u2 = uint8(0), ^uint8(0)
	t1, t2 = bitutil.UnpackUint8s(bitutil.PackUint8s(u1, u2))
	if u1 != t1 || u2 != t2 {
		t.Error("failed packing/unpacking uint8s")
	}

	u1, u2 = ^uint8(0), ^uint8(0)
	t1, t2 = bitutil.UnpackUint8s(bitutil.PackUint8s(u1, u2))
	if u1 != t1 || u2 != t2 {
		t.Error("failed packing/unpacking uint8s")
	}

	u1, u2 = ^uint8(0), uint8(0)
	t1, t2 = bitutil.UnpackUint8s(bitutil.PackUint8s(u1, u2))
	if u1 != t1 || u2 != t2 {
		t.Error("failed packing/unpacking uint8s")
	}
}

func TestPackUint16s(t *testing.T) {
	u1, u2 := uint16(0), uint16(0)
	t1, t2 := bitutil.UnpackUint16s(bitutil.PackUint16s(u1, u2))
	if u1 != t1 || u2 != t2 {
		t.Error("failed packing/unpacking uint16s")
	}

	u1, u2 = uint16(0), ^uint16(0)
	t1, t2 = bitutil.UnpackUint16s(bitutil.PackUint16s(u1, u2))
	if u1 != t1 || u2 != t2 {
		t.Error("failed packing/unpacking uint16s")
	}

	u1, u2 = ^uint16(0), ^uint16(0)
	t1, t2 = bitutil.UnpackUint16s(bitutil.PackUint16s(u1, u2))
	if u1 != t1 || u2 != t2 {
		t.Error("failed packing/unpacking uint16s")
	}

	u1, u2 = ^uint16(0), uint16(0)
	t1, t2 = bitutil.UnpackUint16s(bitutil.PackUint16s(u1, u2))
	if u1 != t1 || u2 != t2 {
		t.Error("failed packing/unpacking uint16s")
	}
}

func TestPackUint32s(t *testing.T) {
	u1, u2 := uint32(0), uint32(0)
	t1, t2 := bitutil.UnpackUint32s(bitutil.PackUint32s(u1, u2))
	if u1 != t1 || u2 != t2 {
		t.Error("failed packing/unpacking uint32s")
	}

	u1, u2 = uint32(0), ^uint32(0)
	t1, t2 = bitutil.UnpackUint32s(bitutil.PackUint32s(u1, u2))
	if u1 != t1 || u2 != t2 {
		t.Error("failed packing/unpacking uint32s")
	}

	u1, u2 = ^uint32(0), ^uint32(0)
	t1, t2 = bitutil.UnpackUint32s(bitutil.PackUint32s(u1, u2))
	if u1 != t1 || u2 != t2 {
		t.Error("failed packing/unpacking uint32s")
	}

	u1, u2 = ^uint32(0), uint32(0)
	t1, t2 = bitutil.UnpackUint32s(bitutil.PackUint32s(u1, u2))
	if u1 != t1 || u2 != t2 {
		t.Error("failed packing/unpacking uint32s")
	}
}
